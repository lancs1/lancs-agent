# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

import sys
from datetime import date
from unittest.mock import MagicMock, patch

import pytest

with patch('lancs.core.common.lancs_uid'):
    with patch('lancs.core.common.lancs_gid'):
        sys.modules['lancs.rbac.orm'] = MagicMock()
        import lancs.rbac.decorators
        import lancs.stats as stats
        from lancs.core.results import AffectedItemsLancsResult
        from lancs.tests.util import RBAC_bypasser

        del sys.modules['lancs.rbac.orm']
        lancs.rbac.decorators.expose_resources = RBAC_bypasser


def test_totals():
    """Verify totals() function works and returns correct data"""
    with patch('lancs.stats.totals_', return_value=({})):
        response = stats.totals(date(2019, 8, 13))
        assert response.total_affected_items == len(response.affected_items)
        assert isinstance(response, AffectedItemsLancsResult), 'The result is not LancsResult type'


def test_hourly():
    """Makes sure hourly() fit with the expected."""
    response = stats.hourly()
    assert isinstance(response, AffectedItemsLancsResult), 'The result is not LancsResult type'
    assert response.total_affected_items == len(response.affected_items)


def test_weekly():
    """Makes sure weekly() fit with the expected."""
    response = stats.weekly()
    assert isinstance(response, AffectedItemsLancsResult), 'The result is not LancsResult type'
    assert response.total_affected_items == len(response.affected_items)


@patch('lancs.stats.get_daemons_stats_', return_value=[{"events_decoded": 1.0}])
def test_get_daemons_stats(mock_daemons_stats_):
    """Makes sure get_daemons_stats() fit with the expected."""
    response = stats.get_daemons_stats('filename')
    assert isinstance(response, AffectedItemsLancsResult), 'The result is not LancsResult type'
    assert response.total_affected_items == len(response.affected_items)


@pytest.mark.parametrize('component', [
    'logcollector', 'test'
])
@patch('lancs.core.agent.Agent.get_stats')
@patch('lancs.stats.get_agents_info', return_value=['000', '001'])
def test_get_agents_component_stats_json(mock_agents_info, mock_getstats, component):
    """Test `get_agents_component_stats_json` function from agent module."""
    response = stats.get_agents_component_stats_json(agent_list=['001'], component=component)
    assert isinstance(response, AffectedItemsLancsResult), 'The result is not AffectedItemsLancsResult type'
    mock_getstats.assert_called_once_with(component=component)


@patch('lancs.core.agent.Agent.get_stats')
@patch('lancs.stats.get_agents_info', return_value=['000', '001'])
def test_get_agents_component_stats_json_ko(mock_agents_info, mock_getstats):
    """Test `get_agents_component_stats_json` function from agent module."""
    response = stats.get_agents_component_stats_json(agent_list=['003'], component='logcollector')
    assert isinstance(response, AffectedItemsLancsResult), 'The result is not AffectedItemsLancsResult type'
    assert response.render()['data']['failed_items'][0]['error']['code'] == 1701, 'Expected error code was not returned'
