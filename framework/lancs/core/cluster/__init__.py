

# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

__version__ = '4.3.10'
__revision__ = '40323'
__author__ = "Lancs Inc"
__lancs_name__ = "Lancs"
__licence__ = "\
This program is free software; you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License (version 2) as \n\
published by the Free Software Foundation. For more details, go to \n\
https://www.gnu.org/licenses/gpl.html\n"
