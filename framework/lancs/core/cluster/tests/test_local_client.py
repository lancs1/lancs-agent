# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

import asyncio
from unittest.mock import patch

import pytest
import uvloop

with patch('lancs.common.lancs_uid'):
    with patch('lancs.common.lancs_gid'):
        from lancs.core.exception import LancsException
        from lancs.core.cluster.local_client import LocalClient

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
loop = uvloop.new_event_loop()


@patch.object(loop, attribute='create_unix_connection', side_effect=MemoryError)
@patch('asyncio.get_running_loop', return_value=loop)
def test_crypto(mock_runningloop, mock_loop):
    with pytest.raises(LancsException, match=".* 1119 .*"):
        local_client = LocalClient()
        loop.run_until_complete(local_client.start())
