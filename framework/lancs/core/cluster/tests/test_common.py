import json
import os
import sys
from unittest.mock import patch, MagicMock

import pytest

with patch('lancs.common.getgrnam'):
    with patch('lancs.common.getpwnam'):
        with patch('lancs.common.lancs_uid'):
            with patch('lancs.common.lancs_gid'):
                sys.modules['lancs.rbac.orm'] = MagicMock()

                from lancs.core.cluster.cluster import get_node
                from lancs.agent import get_agents_summary_status
                from lancs.core.exception import LancsError, LancsInternalError
                from lancs.core.manager import status
                from lancs.core.results import LancsResult, AffectedItemsLancsResult
                from lancs.core.cluster.common import LancsJSONEncoder, as_lancs_object

affected = AffectedItemsLancsResult(dikt={'data': ['test']}, affected_items=['001', '002'])
affected.add_failed_item(id_='099', error=LancsError(1750, extra_message='wiiiiiii'))
affected.add_failed_item(id_='111', error=LancsError(1750, extra_message='weeeeee'))
affected.add_failed_item(id_='555', error=LancsError(1750, extra_message='wiiiiiii'))
affected.add_failed_item(id_='333', error=LancsError(1707, extra_message='wiiiiiii'))

with patch('lancs.common.lancs_path', new=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')):
    objects_to_encode = [
        {'foo': 'bar',
         'foo2': 3,
         'mycallable': get_node},
        {'foo': 'bar',
         'foo2': 3,
         'mycallable': get_agents_summary_status},
        {'foo': 'bar',
         'foo2': 3,
         'mycallable': status},
        {'foo': 'bar',
         'foo2': 3,
         'exception': LancsError(1500,
                                 extra_message="test message",
                                 extra_remediation="test remediation")},
        {'foo': 'bar',
         'foo2': 3,
         'exception': LancsInternalError(1000,
                                         extra_message="test message",
                                         extra_remediation="test remediation")},
        {'foo': 'bar',
         'foo2': 3,
         'result': LancsResult({'field1': 'value1', 'field2': 3}, str_priority=['KO', 'OK'])},
        {'foo': 'bar',
         'foo2': 3,
         'result': affected}
    ]


@pytest.mark.parametrize('obj', objects_to_encode)
@patch('lancs.common.lancs_path', new=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data'))
def test_encoder_decoder(obj):
    # Encoding first object
    encoded = json.dumps(obj, cls=LancsJSONEncoder)

    # Decoding first object
    obj_again = json.loads(encoded, object_hook=as_lancs_object)
    assert (obj_again == obj)
