import json
from contextvars import ContextVar
from grp import getgrnam
from pwd import getpwnam
from unittest.mock import patch

import pytest

from lancs.core.common import find_lancs_path, lancs_uid, lancs_gid, context_cached, reset_context_cache, \
    get_context_cache


@pytest.mark.parametrize('fake_path, expected', [
    ('/var/lancsnet/framework/python/lib/python3.7/site-packages/lancs-3.10.0-py3.7.egg/lancs', '/var/lancsnet'),
    ('/my/custom/path/framework/python/lib/python3.7/site-packages/lancs-3.10.0-py3.7.egg/lancs', '/my/custom/path'),
    ('/my/fake/path', '')
])
def test_find_lancs_path(fake_path, expected):
    with patch('lancs.core.common.__file__', new=fake_path):
        assert(find_lancs_path.__wrapped__() == expected)


def test_find_lancs_path_relative_path():
    with patch('os.path.abspath', return_value='~/framework'):
        assert(find_lancs_path.__wrapped__() == '~')


def test_lancs_uid():
    with patch('lancs.core.common.getpwnam', return_value=getpwnam("root")):
        lancs_uid()


def test_lancs_gid():
    with patch('lancs.core.common.getgrnam', return_value=getgrnam("root")):
        lancs_gid()


def test_context_cached():
    """Verify that context_cached decorator correctly saves and returns saved value when called again."""

    test_context_cached.calls_to_foo = 0

    @context_cached('foobar')
    def foo(arg='bar', **data):
        test_context_cached.calls_to_foo += 1
        return arg

    # The result of function 'foo' is being cached and it has been called once
    assert foo() == 'bar' and test_context_cached.calls_to_foo == 1, '"bar" should be returned with 1 call to foo.'
    assert foo() == 'bar' and test_context_cached.calls_to_foo == 1, '"bar" should be returned with 1 call to foo.'
    assert isinstance(get_context_cache()[json.dumps({"key": "foobar", "args": [], "kwargs": {}})], ContextVar)

    # foo called with an argument
    assert foo('other_arg') == 'other_arg' and test_context_cached.calls_to_foo == 2, '"other_arg" should be ' \
                                                                                      'returned with 2 calls to foo. '
    assert isinstance(get_context_cache()[json.dumps({"key": "foobar", "args": ['other_arg'], "kwargs": {}})],
                      ContextVar)

    # foo called with the same argument as default, a new context var is created in the cache
    assert foo('bar') == 'bar' and test_context_cached.calls_to_foo == 3, '"bar" should be returned with 3 calls to ' \
                                                                          'foo. '
    assert isinstance(get_context_cache()[json.dumps({"key": "foobar", "args": ['bar'], "kwargs": {}})], ContextVar)

    # Reset cache and calls to foo
    reset_context_cache()
    test_context_cached.calls_to_foo = 0

    # foo called with kwargs, a new context var is created with kwargs not empty
    assert foo(data='bar') == 'bar' and test_context_cached.calls_to_foo == 1, '"bar" should be returned with 1 ' \
                                                                               'calls to foo. '
    assert isinstance(get_context_cache()[json.dumps({"key": "foobar", "args": [], "kwargs": {"data": "bar"}})],
                      ContextVar)
