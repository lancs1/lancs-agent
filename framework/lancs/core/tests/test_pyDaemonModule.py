# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

from unittest.mock import patch
import pytest

from lancs.core.pyDaemonModule import *
from lancs.core.exception import LancsException
from tempfile import NamedTemporaryFile, TemporaryDirectory


@pytest.mark.parametrize('effect', [
   None,
   OSError(10000, 'Error')
])
@patch('lancs.core.pyDaemonModule.sys.exit')
@patch('lancs.core.pyDaemonModule.os.setsid')
@patch('lancs.core.pyDaemonModule.sys.stderr.write')
@patch('lancs.core.pyDaemonModule.sys.stdin.fileno')
@patch('lancs.core.pyDaemonModule.os.dup2')
@patch('lancs.core.pyDaemonModule.os.chdir')
def test_pyDaemon(mock_chdir, mock_dup, mock_fileno, mock_write, mock_setsid, mock_exit, effect):
    """Tests pyDaemon function works"""

    with patch('lancs.core.pyDaemonModule.os.fork', return_value=255, side_effect=effect):
        pyDaemon()

    if effect == None:
        mock_exit.assert_called_with(0)
    else:
        mock_exit.assert_called_with(1)
    mock_setsid.assert_called_once_with()
    mock_chdir.assert_called_once_with('/')


@patch('lancs.core.pyDaemonModule.common.lancs_path', new='/tmp')
def test_create_pid():
    """Tests create_pid function works"""

    with TemporaryDirectory() as tmpdirname:
        tmpfile = NamedTemporaryFile(dir=tmpdirname, delete=False, suffix='-255.pid')
        with patch('lancs.core.pyDaemonModule.common.os_pidfile', new=tmpdirname.split('/')[2]):
            create_pid(tmpfile.name.split('/')[3].split('-')[0], '255')


@patch('lancs.core.pyDaemonModule.common.lancs_path', new='/tmp')
@patch('lancs.core.pyDaemonModule.os.chmod', side_effect=OSError)
def test_create_pid_ko(mock_chmod):
    """Tests create_pid function exception works"""

    with TemporaryDirectory() as tmpdirname:
        tmpfile = NamedTemporaryFile(dir=tmpdirname, delete=False, suffix='-255.pid')
        with patch('lancs.core.pyDaemonModule.common.os_pidfile', new=tmpdirname.split('/')[2]):
            with pytest.raises(LancsException, match=".* 3002 .*"):
                create_pid(tmpfile.name.split('/')[3].split('-')[0], '255')


@patch('lancs.core.pyDaemonModule.common.lancs_path', new='/tmp')
def test_delete_pid():
    """Tests delete_pid function works"""

    with TemporaryDirectory() as tmpdirname:
        tmpfile = NamedTemporaryFile(dir=tmpdirname, delete=False, suffix='-255.pid')
        with patch('lancs.core.pyDaemonModule.common.os_pidfile', new=tmpdirname.split('/')[2]):
            delete_pid(tmpfile.name.split('/')[3].split('-')[0], '255')
