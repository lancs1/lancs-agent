# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

from unittest.mock import patch

import pytest

with patch('lancs.common.lancs_uid'):
    with patch('lancs.common.lancs_gid'):
        from lancs.core.logtest import send_logtest_msg
        from lancs.core.common import LOGTEST_SOCKET


@pytest.mark.parametrize('params', [
    {'command': 'random_command', 'parameters': {'param1': 'value1'}},
    {'command': None, 'parameters': None}
])
@patch('lancs.core.logtest.LancsSocketJSON.__init__', return_value=None)
@patch('lancs.core.logtest.LancsSocketJSON.send')
@patch('lancs.core.logtest.LancsSocketJSON.close')
@patch('lancs.core.logtest.create_lancs_socket_message')
def test_send_logtest_msg(create_message_mock, close_mock, send_mock, init_mock, params):
    """Test `send_logtest_msg` function from module core.logtest.

    Parameters
    ----------
    params : dict
        Params that will be sent to the logtest socket.
    """
    with patch('lancs.core.logtest.LancsSocketJSON.receive',
               return_value={'data': {'response': True, 'output': {'timestamp': '1970-01-01T00:00:00.000000-0200'}}}):
        response = send_logtest_msg(**params)
        init_mock.assert_called_with(LOGTEST_SOCKET)
        create_message_mock.assert_called_with(origin={'name': 'Logtest', 'module': 'framework'}, **params)
        assert response == {'data': {'response': True, 'output': {'timestamp': '1970-01-01T02:00:00.000000Z'}}}

