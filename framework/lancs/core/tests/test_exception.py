# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

from lancs.core.exception import LancsException, LancsError

def test_lancs_exception__or__():
    """Check that LancsException's | operator performs the join of dapi errors properly."""
    excp1 = LancsException(1308)
    excp1._dapi_errors = {'test1': 'test error'}
    excp2 = LancsException(1308)
    excp2._dapi_errors = {'test2': 'test error'}
    excp3 = excp2 | excp1
    assert excp3._dapi_errors == {'test1': 'test error', 'test2': 'test error'}


def test_lancs_exception__deepcopy__():
    """Check that LancsException's __deepcopy__ magic method works properly."""
    excp1 = LancsException(1308)
    excp2 = excp1.__deepcopy__()
    assert excp1 == excp2 and excp1 is not excp2


def test_lancs_error__or__():
    """Check that LancsError's | operator performs the union of id sets properly."""
    error1 = LancsError(1309, ids={1, 2, 3})
    error2 = LancsError(1309, ids={4, 5, 6})
    error3 = error2 | error1
    assert error3.ids == {1, 2, 3, 4, 5, 6}
