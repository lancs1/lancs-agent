# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

from unittest.mock import patch, MagicMock

import pytest

from lancs.core.exception import LancsException
from lancs.core.lancs_socket import LancsSocket, LancsSocketJSON, SOCKET_COMMUNICATION_PROTOCOL_VERSION, \
    create_lancs_socket_message


@patch('lancs.core.lancs_socket.LancsSocket._connect')
def test_LancsSocket__init__(mock_conn):
    """Tests LancsSocket.__init__ function works"""

    LancsSocket('test_path')

    mock_conn.assert_called_once_with()


@patch('lancs.core.lancs_socket.socket.socket.connect')
def test_LancsSocket_protected_connect(mock_conn):
    """Tests LancsSocket._connect function works"""

    LancsSocket('test_path')

    mock_conn.assert_called_with('test_path')


@patch('lancs.core.lancs_socket.socket.socket.connect', side_effect=Exception)
def test_LancsSocket_protected_connect_ko(mock_conn):
    """Tests LancsSocket._connect function exceptions works"""

    with pytest.raises(LancsException, match=".* 1013 .*"):
        LancsSocket('test_path')


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.socket.socket.close')
def test_LancsSocket_close(mock_close, mock_conn):
    """Tests LancsSocket.close function works"""

    queue = LancsSocket('test_path')

    queue.close()

    mock_conn.assert_called_once_with('test_path')
    mock_close.assert_called_once_with()


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.socket.socket.send')
def test_LancsSocket_send(mock_send, mock_conn):
    """Tests LancsSocket.send function works"""

    queue = LancsSocket('test_path')

    response = queue.send(b"\x00\x01")

    assert isinstance(response, MagicMock)
    mock_conn.assert_called_once_with('test_path')


@pytest.mark.parametrize('msg, effect, send_effect, expected_exception', [
    ('text_msg', 'side_effect', None, 1105),
    (b"\x00\x01", 'return_value', 0, 1014),
    (b"\x00\x01", 'side_effect', Exception, 1014)
])
@patch('lancs.core.lancs_socket.socket.socket.connect')
def test_LancsSocket_send_ko(mock_conn, msg, effect, send_effect, expected_exception):
    """Tests LancsSocket.send function exceptions works"""

    queue = LancsSocket('test_path')

    if effect == 'return_value':
        with patch('lancs.core.lancs_socket.socket.socket.send', return_value=send_effect):
            with pytest.raises(LancsException, match=f'.* {expected_exception} .*'):
                queue.send(msg)
    else:
        with patch('lancs.core.lancs_socket.socket.socket.send', side_effect=send_effect):
            with pytest.raises(LancsException, match=f'.* {expected_exception} .*'):
                queue.send(msg)

    mock_conn.assert_called_once_with('test_path')


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.unpack', return_value='1024')
@patch('lancs.core.lancs_socket.socket.socket.recv')
def test_LancsSocket_receive(mock_recv, mock_unpack, mock_conn):
    """Tests LancsSocket.receive function works"""

    queue = LancsSocket('test_path')

    response = queue.receive()

    assert isinstance(response, MagicMock)
    mock_conn.assert_called_once_with('test_path')


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.socket.socket.recv', side_effect=Exception)
def test_LancsSocket_receive_ko(mock_recv, mock_conn):
    """Tests LancsSocket.receive function exception works"""

    queue = LancsSocket('test_path')

    with pytest.raises(LancsException, match=".* 1014 .*"):
        queue.receive()

    mock_conn.assert_called_once_with('test_path')


@patch('lancs.core.lancs_socket.LancsSocket._connect')
def test_LancsSocketJSON__init__(mock_conn):
    """Tests LancsSocketJSON.__init__ function works"""

    LancsSocketJSON('test_path')

    mock_conn.assert_called_once_with()


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.LancsSocket.send')
def test_LancsSocketJSON_send(mock_send, mock_conn):
    """Tests LancsSocketJSON.send function works"""

    queue = LancsSocketJSON('test_path')

    response = queue.send('test_msg')

    assert isinstance(response, MagicMock)
    mock_conn.assert_called_once_with('test_path')


@pytest.mark.parametrize('raw', [
    True, False
])
@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.LancsSocket.receive')
@patch('lancs.core.lancs_socket.loads', return_value={'error':0, 'message':None, 'data':'Ok'})
def test_LancsSocketJSON_receive(mock_loads, mock_receive, mock_conn, raw):
    """Tests LancsSocketJSON.receive function works"""
    queue = LancsSocketJSON('test_path')
    response = queue.receive(raw=raw)
    if raw:
        assert isinstance(response, dict)
    else:
        assert isinstance(response, str)
    mock_conn.assert_called_once_with('test_path')


@patch('lancs.core.lancs_socket.socket.socket.connect')
@patch('lancs.core.lancs_socket.LancsSocket.receive')
@patch('lancs.core.lancs_socket.loads', return_value={'error':10000, 'message':'Error', 'data':'KO'})
def test_LancsSocketJSON_receive_ko(mock_loads, mock_receive, mock_conn):
    """Tests LancsSocketJSON.receive function works"""

    queue = LancsSocketJSON('test_path')

    with pytest.raises(LancsException, match=".* 10000 .*"):
        queue.receive()

    mock_conn.assert_called_once_with('test_path')


@pytest.mark.parametrize('origin, command, parameters', [
    ('origin_sample', 'command_sample', {'sample': 'sample'}),
    (None, 'command_sample', {'sample': 'sample'}),
    ('origin_sample', None, {'sample': 'sample'}),
    ('origin_sample', 'command_sample', None),
    (None, None, None)
])
def test_create_lancs_socket_message(origin, command, parameters):
    """Test create_lancs_socket_message function."""
    response_message = create_lancs_socket_message(origin, command, parameters)
    assert response_message['version'] == SOCKET_COMMUNICATION_PROTOCOL_VERSION
    assert response_message.get('origin') == origin
    assert response_message.get('command') == command
    assert response_message.get('parameters') == parameters
