# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

from unittest.mock import patch

import pytest

from lancs.core.exception import LancsException
from lancs.core.lancs_queue import LancsQueue


@patch('lancs.core.lancs_queue.LancsQueue._connect')
def test_LancsQueue__init__(mock_conn):
    """Test LancsQueue.__init__ function."""

    LancsQueue('test_path')

    mock_conn.assert_called_once_with()


@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.socket.socket.setsockopt')
def test_LancsQueue_protected_connect(mock_set, mock_conn):
    """Test LancsQueue._connect function."""

    LancsQueue('test_path')

    with patch('lancs.core.lancs_queue.socket.socket.getsockopt', return_value=1):
        LancsQueue('test_path')

    mock_conn.assert_called_with('test_path')
    mock_set.assert_called_once_with(1, 7, 6400)


@patch('lancs.core.lancs_queue.socket.socket.connect', side_effect=Exception)
def test_LancsQueue_protected_connect_ko(mock_conn):
    """Test LancsQueue._connect function exceptions."""

    with pytest.raises(LancsException, match=".* 1010 .*"):
        LancsQueue('test_path')


@pytest.mark.parametrize('send_response, error', [
    (1, False),
    (0, True)
])
@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.LancsQueue.MAX_MSG_SIZE', new=0)
def test_LancsQueue_protected_send(mock_conn, send_response, error):
    """Test LancsQueue._send function.

    Parameters
    ----------
    send_response : int
        Returned value of the socket send mocked function.
    error : bool
        Indicates whether a LancsException will be raised or not.
    """

    queue = LancsQueue('test_path')

    with patch('socket.socket.send', return_value=send_response):
        if error:
            with pytest.raises(LancsException, match=".* 1011 .*"):
                queue._send('msg')
        else:
            queue._send('msg')

    mock_conn.assert_called_with('test_path')


@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.LancsQueue.MAX_MSG_SIZE', new=0)
@patch('socket.socket.send', side_effect=Exception)
def test_LancsQueue_protected_send_ko(mock_send, mock_conn):
    """Test LancsQueue._send function exceptions."""

    queue = LancsQueue('test_path')

    with pytest.raises(LancsException, match=".* 1011 .*"):
        queue._send('msg')

    mock_conn.assert_called_with('test_path')


@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.socket.socket.close')
def test_LancsQueue_close(mock_close, mock_conn):
    """Test LancsQueue.close function."""

    queue = LancsQueue('test_path')

    queue.close()

    mock_conn.assert_called_once_with('test_path')
    mock_close.assert_called_once_with()


@pytest.mark.parametrize('msg, agent_id, msg_type', [
    ('test_msg', '000', 'ar-message'),
    ('test_msg', '001', 'ar-message'),
    ('test_msg', None, 'ar-message'),
    ('syscheck restart', '000', None),
    ('force_reconnect', '000', None),
    ('restart-lancsnet0', '001', None),
    ('syscheck restart', None, None),
    ('force_reconnect', None, None),
    ('restart-lancsnet0', None, None)
])
@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.LancsQueue._send')
def test_LancsQueue_send_msg_to_agent(mock_send, mock_conn, msg, agent_id, msg_type):
    """Test LancsQueue.send_msg_to_agent function.

    Parameters
    ----------
    msg : str
        Message sent to the agent.
    agent_id : str
        String indicating the agent ID.
    msg_type : str
        String indicating the message type.
    """

    queue = LancsQueue('test_path')

    response = queue.send_msg_to_agent(msg, agent_id, msg_type)

    assert isinstance(response, str)
    mock_conn.assert_called_once_with('test_path')


@pytest.mark.parametrize('msg, agent_id, msg_type, expected_exception', [
    ('test_msg', '000', None, 1012),
    ('syscheck restart', None, None, 1014),
])
@patch('lancs.core.lancs_queue.socket.socket.connect')
@patch('lancs.core.lancs_queue.LancsQueue._send', side_effect=Exception)
def test_LancsQueue_send_msg_to_agent_ko(mock_send, mock_conn, msg, agent_id, msg_type, expected_exception):
    """Test LancsQueue.send_msg_to_agent function exceptions.

    Parameters
    ----------
    msg : str
        Message sent to the agent.
    agent_id : str
        String indicating the agent ID.
    msg_type : str
        String indicating the message type.
    expected_exception : int
        Expected Lancs exception.
    """

    queue = LancsQueue('test_path')

    with pytest.raises(LancsException, match=f'.* {expected_exception} .*'):
        queue.send_msg_to_agent(msg, agent_id, msg_type)

    mock_conn.assert_called_once_with('test_path')
