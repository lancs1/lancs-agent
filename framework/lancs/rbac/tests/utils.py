import os
from importlib import reload
from unittest.mock import patch

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError


def create_memory_db(sql_file, session, test_data_path):
    with open(os.path.join(test_data_path, sql_file)) as f:
        for line in f.readlines():
            line = line.strip()
            if '* ' not in line and '/*' not in line and '*/' not in line and line != '':
                session.execute(line)
                session.commit()


def init_db(schema, test_data_path):
    with patch('lancs.core.common.lancs_uid'), patch('lancs.core.common.lancs_gid'):
        with patch('sqlalchemy.create_engine', return_value=create_engine("sqlite://")):
            with patch('shutil.chown'), patch('os.chmod'):
                with patch('api.constants.SECURITY_PATH', new=test_data_path):
                    import lancs.rbac.orm as orm
                    reload(orm)
                    orm.create_rbac_db()
    try:
        create_memory_db(schema, orm._Session(), test_data_path)
    except OperationalError:
        pass
