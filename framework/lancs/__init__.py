

# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

from datetime import datetime
from time import strftime

from lancs.core import common
from lancs.core.wdb import LancsDBConnection
from lancs.core.exception import LancsException, LancsError, LancsInternalError

"""
Lancs HIDS Python package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Lancs is a python package to manage LANCSNET.

"""

__version__ = '4.3.10'


msg = "\n\nPython 2.7 or newer not found."
msg += "\nUpdate it or set the path to a valid version. Example:"
msg += "\n  export PATH=$PATH:/opt/rh/python27/root/usr/bin"
msg += "\n  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/rh/python27/root/usr/lib64"

try:
    from sys import version_info as python_version
    if python_version.major < 2 or (python_version.major == 2 and python_version.minor < 7):
        raise LancsInternalError(999, msg)
except Exception as e:
    raise LancsInternalError(999, msg)


class Lancs:
    """
    Basic class to set up LANCSNET directories
    """

    def __init__(self):
        """
        Initialize basic information and directories.
        :return:
        """

        self.version = common.lancs_version
        self.installation_date = common.installation_date
        self.type = common.install_type
        self.path = common.lancs_path
        self.max_agents = 'unlimited'
        self.openssl_support = 'N/A'
        self.tz_offset = None
        self.tz_name = None

        self._initialize()

    def __str__(self):
        return str(self.to_dict())

    def __eq__(self, other):
        if isinstance(other, Lancs):
            return self.to_dict() == other.to_dict()
        return False

    def to_dict(self):
        date_format = '%a %b %d %H:%M:%S %Z %Y'
        try:
            compilation_date = datetime.strptime(self.installation_date, date_format)
        except ValueError:
            compilation_date = datetime.now()
        return {'path': self.path,
                'version': self.version,
                'compilation_date': compilation_date,
                'type': self.type,
                'max_agents': self.max_agents,
                'openssl_support': self.openssl_support,
                'tz_offset': self.tz_offset,
                'tz_name': self.tz_name
                }

    def _initialize(self):
        """
        Calculates all Lancs installation metadata
        """
        # info DB if possible
        try:
            wdb_conn = LancsDBConnection()
            open_ssl = wdb_conn.execute("global sql SELECT value FROM info WHERE key = 'openssl_support'")[0]['value']
            self.openssl_support = open_ssl
        except Exception:
            self.openssl_support = "N/A"

        # Timezone info
        try:
            self.tz_offset = strftime("%z")
            self.tz_name = strftime("%Z")
        except Exception:
            self.tz_offset = None
            self.tz_name = None

        return self.to_dict()


def main():
    print("Lancs HIDS Library")
