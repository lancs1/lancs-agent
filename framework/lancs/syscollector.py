# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

from lancs.core import common
from lancs.core.agent import get_agents_info
from lancs.core.exception import LancsError, LancsResourceNotFound
from lancs.core.results import AffectedItemsLancsResult, merge
from lancs.core.syscollector import LancsDBQuerySyscollector, get_valid_fields, Type
from lancs.rbac.decorators import expose_resources


@expose_resources(actions=['syscollector:read'], resources=['agent:id:{agent_list}'])
def get_item_agent(agent_list, offset=0, limit=common.database_limit, select=None, search=None, sort=None, filters=None,
                   q='', array=True, nested=True, element_type='os'):
    """ Get syscollector information about a list of agents.

    :param agent_list: List of agents ID's.
    :param offset: First item to return.
    :param limit: Maximum number of items to return.
    :param sort: Sorts the items. Format: {"fields":["field1","field2"],"order":"asc|desc"}.
    :param select: Select fields to return. Format: {"fields":["field1","field2"]}.
    :param search: Looks for items with the specified string. Format: {"fields": ["field1","field2"]}
    :param q: Defines query to filter in DB.
    :param filters: Fields to filter by
    :param nested: Nested fields
    :param array: Array
    :param element_type: Type of element to get syscollector information from
    :return: AffectedItemsLancsResult
    """
    result = AffectedItemsLancsResult(
        none_msg='No syscollector information was returned',
        some_msg='Some syscollector information was not returned',
        all_msg='All specified syscollector information was returned',
        sort_fields=['agent_id'] if sort is None else sort['fields'],
        sort_casting=['str'],
        sort_ascending=[sort['order'] == 'asc' for _ in sort['fields']] if sort is not None else ['True']
    )

    system_agents = get_agents_info()
    for agent in agent_list:
        try:
            if agent not in system_agents:
                raise LancsResourceNotFound(1701)
            table, valid_select_fields = get_valid_fields(Type(element_type), agent_id=agent)
            with LancsDBQuerySyscollector(agent_id=agent, offset=offset, limit=limit, select=select,
                                          search=search,
                                          sort=sort, filters=filters, fields=valid_select_fields, table=table,
                                          array=array, nested=nested, query=q) as db_query:
                data = db_query.run()

            for item in data['items']:
                item['agent_id'] = agent
                result.affected_items.append(item)
            result.total_affected_items += data['totalItems']
        except LancsResourceNotFound as e:
            result.add_failed_item(id_=agent, error=e)

    # Avoid that integer type fields are casted to string, this prevents sort parameter malfunctioning
    try:
        if len(result.affected_items) and sort and len(sort['fields']) == 1:
            fields = sort['fields'][0].split('.')
            element = result.affected_items[0][fields.pop(0)]
            for field in fields:
                element = element[field]
            element_type = type(element).__name__
            result.sort_casting = [element_type] if element_type not in ['str', 'datetime'] else ['str']
    except KeyError:
        pass

    result.affected_items = merge(*[[res] for res in result.affected_items],
                                  criteria=result.sort_fields,
                                  ascending=result.sort_ascending,
                                  types=result.sort_casting)

    return result
