# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute it and/or modify it under the terms of GPLv2

import os
import subprocess
from functools import lru_cache


@lru_cache(maxsize=None)
def find_lancs_path():
    """
    Gets the path where Lancs is installed dinamically

    :return: str path where Lancs is installed or empty string if there is no framework in the environment
    """
    abs_path = os.path.abspath(os.path.dirname(__file__))
    allparts = []
    while 1:
        parts = os.path.split(abs_path)
        if parts[0] == abs_path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == abs_path:  # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            abs_path = parts[0]
            allparts.insert(0, parts[1])

    lancs_path = ''
    try:
        for i in range(0, allparts.index('wodles')):
            lancs_path = os.path.join(lancs_path, allparts[i])
    except ValueError:
        pass

    return lancs_path


def call_lancs_control(option) -> str:
    lancs_control = os.path.join(find_lancs_path(), "bin", "lancs-control")
    try:
        proc = subprocess.Popen([lancs_control, option], stdout=subprocess.PIPE)
        (stdout, stderr) = proc.communicate()
        return stdout.decode()
    except Exception:
        pass


def get_lancs_info(field) -> str:
    lancs_info = call_lancs_control("info")
    if not lancs_info:
        return "ERROR"

    if not field:
        return lancs_info

    env_variables = lancs_info.rsplit("\n")
    env_variables.remove("")
    lancs_env_vars = dict()
    for env_variable in env_variables:
        key, value = env_variable.split("=")
        lancs_env_vars[key] = value.replace("\"", "")

    return lancs_env_vars[field]


@lru_cache(maxsize=None)
def get_lancs_version() -> str:
    return get_lancs_info("LANCS_VERSION")


ANALYSISD = os.path.join(find_lancs_path(), 'queue', 'sockets', 'queue')
