#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute
# it and/or modify it under the terms of GPLv2

"""Unit tests for subscriber module."""

import os
import sys
from unittest.mock import patch

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))  # noqa: E501
from pubsub.subscriber import LancsGCloudSubscriber

test_data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

credentials_file = 'credentials.json'
logger = None
project = 'lancs-dev'
subscription_id = 'testing'
test_message = 'test-message'.encode()


@patch('pubsub.subscriber.pubsub.subscriber.Client.from_service_account_file')
def get_lancsgcloud_subscriber(mock_client):
    """Return a LancsGCloudSubscriber client."""
    client = LancsGCloudSubscriber(credentials_file, logger, project, subscription_id)
    return client


def test_get_subscriber():
    """Check if an instance of LancsGCloudSubscriber is created properly."""
    expected_attributes = ['logger', 'subscriber', 'subscription_path']

    client = get_lancsgcloud_subscriber()

    assert isinstance(client, LancsGCloudSubscriber)

    for attribute in expected_attributes:
        assert hasattr(client, attribute)
