#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is free software; you can redistribute
# it and/or modify it under the terms of GPLv2

"""Unit tests for bucket module."""

import os
import sys
from unittest.mock import patch

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))  # noqa: E501
from buckets.bucket import LancsGCloudBucket

test_data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

credentials_file = 'credentials.json'
logger = None
bucket_name = 'test-bucket'
prefix = ""
delete_file = False
only_logs_after = None


@patch('buckets.bucket.storage.client.Client.from_service_account_json')
def get_LancsGCloudBucket(mock_client):
    """Return a LancsGCloudSubscriber client."""
    client = LancsGCloudBucket(credentials_file, logger, bucket_name, prefix, delete_file, only_logs_after)
    return client


def test_get_bucket():
    """Check if an instance of LancsGCloudBucket is created properly."""
    expected_attributes = ['bucket_name', 'bucket', 'client', 'project_id', 'prefix', 'delete_file', 'only_logs_after',
                           'db_connector', 'datetime_format']

    client = get_LancsGCloudBucket()

    assert isinstance(client, LancsGCloudBucket)

    for attribute in expected_attributes:
        assert hasattr(client, attribute)
