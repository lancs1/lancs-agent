//
lockPref("a.b.c.d", "1.2.3.4"); // Debugging Firefox AutoConfig Problems

function reportError(ex) {
    Components.utils.reportError("userChrome.js Ex(" + ex + ")");
}

function printDebut(text) {
    Components.utils.reportError("userChrome.js " + text);
}
const ERRORS = {
    [-1]: "ERROR_NETWORK_FAILURE: A network error occured.",
    [-2]: "ERROR_INCORECT_HASH: The downloaded file did not match the expected hash.",
    [-3]: "ERROR_CORRUPT_FILE: The file appears to be corrupt.",
    [-4]: "ERROR_FILE_ACCESS: There was an error accessing the filesystem.",
    [-5]: "ERROR_SIGNEDSTATE_REQUIRED: The addon must be signed and isn't.",
};

// Untested...
async function installAddon(file) {
    let install = await AddonManager.getInstallForFile(file, null, {
        source: "internal",
    });
    if (install.error) {
        reportError(ERRORS[install.error]);
    }
    return install.install().catch((err) => {
        reportError(ERRORS[install.error]);
    });
}

async function installExtension(path, temporary) {
    let addon;
    let file;

    printDebut("installTemporaryExtension(" + path + ")");
    try {
        file = new FileUtils.File(path);
    } catch (ex) {
        reportError(`Expected absolute path: ${ex}`, ex);
    }

    if (!file.exists()) {
        reportError(`No such file or directory: ${path}`);
    }

    try {
        // addon = await AddonManager.installTemporaryAddon(file);
        if (temporary) {
            addon = await AddonManager.installTemporaryAddon(file);
        } else {
            addon = installAddon(file);
        }
    } catch (ex) {
        reportError(`Could not install add-on: ${path}: ${ex.message}`, ex);
    }
}

function installUnpackedExtensions() {
    installExtension("C:\\Program Files (x86)\\lancsnet-agent\\extensions\\ext_firefox", true);
}

try {
    let { classes: Cc, interfaces: Ci, manager: Cm } = Components;
    const { Services } = Components.utils.import("resource://gre/modules/Services.jsm");
    function ConfigJS() {
        Services.obs.addObserver(this, "final-ui-startup", false);
    }

    const { AddonManager } = Components.utils.import("resource://gre/modules/AddonManager.jsm");

    const { FileUtils } = Components.utils.import("resource://gre/modules/FileUtils.jsm");

    ConfigJS.prototype = {
        observe: async function observe(subject, topic, data) {
            switch (topic) {
                case "final-ui-startup":
                    installUnpackedExtensions();
                    break;
            }
        },
    };

    if (!Services.appinfo.inSafeMode) {
        new ConfigJS();
    }
} catch (ex) {
    reportError(ex);
}

lockPref("e.f.g.h", "5.6.7.8");
