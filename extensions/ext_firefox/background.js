const BS = chrome || browser;
const server_pre = 'http://localhost:3000';
const lsLinksKey = 'url-terminator-links';
const url = BS.runtime.getURL('identification_ff/client_key.json');

const removeTrailingSlash = url => url?.replace(/\/+$/, '');
const removeHTTPProtocol = url => url?.replace(/https?:\/\//, '');
const removeWWW = url => url?.replace(/www\./, '');
const removeEscapeSymbol = url => url?.replace(/^!/, '');

const sanitizeUrl = url =>
    [url]
        .map(url => removeTrailingSlash(url))
        .map(url => removeHTTPProtocol(url))
        .map(url => removeWWW(url))
        .pop();

const setLocalStorageLinks = links => BS.storage.sync.set({ [lsLinksKey]: links || [] });

const getLocalStorageLinks = async () => {
    const { [lsLinksKey]: links } = await BS.storage.sync.get(lsLinksKey);
    return Array.isArray(links) ? links : [];
};

const getEscapedLinks = blockedLinks =>
    blockedLinks.reduce(
        (arr, bl) => (bl[0] === '!' ? arr.concat(removeEscapeSymbol(bl)) : arr),
        []
    );

const getCurrentTab = async () => {
    const options = { active: true, currentWindow: true };
    const [tab] = await BS.tabs.query(options);
    return tab;
};

const removeTab = async tabId => {
    try {
        await BS.tabs.remove(tabId);
    } catch (e) {
        console.error(e);
    }
};

const checkForBlockedLinks = async (tabId, tabUrl) => {
    const blockedLinks = await getLocalStorageLinks();
    const escapedLinks = getEscapedLinks(blockedLinks);

    if (escapedLinks.find(el => sanitizeUrl(tabUrl)?.includes(sanitizeUrl(el)))) return;

    if (blockedLinks.find(bll => sanitizeUrl(tabUrl)?.includes(sanitizeUrl(bll))))
        await removeTab(tabId);
};

const identifyExtension = async url => {
    const res = await fetch(url);
    const dataJson = await res.json();
    let client_key = dataJson.client_key.split(' ').at(-1);
    return client_key;
};

let clientKey, server;

const fetchData = async () => {
    const blockedLinks = await getLocalStorageLinks();
    clientKey = await identifyExtension(url);
    server = `${server_pre}/${clientKey}`;
    const res = await fetch(server);
    const res_json = await res.json();
    const data = res_json.slice();
    if (blockedLinks.length <= data.length) {
        data.forEach(ele => {
            if (!blockedLinks.find(bll => sanitizeUrl(ele)?.includes(sanitizeUrl(bll)))) {
                console.log(ele);
                blockedLinks.push(ele);
            }
        });
    } else if (blockedLinks.length >= data.length) {
        const dataSanitize = data.map(ele => sanitizeUrl(ele));
        const rm = [];
        blockedLinks.forEach((bll, index) => {
            if (!dataSanitize.find(ele => ele === sanitizeUrl(bll))) rm.push(index);
        });
        rm.slice()
            .reverse()
            .forEach(idx => blockedLinks.splice(idx, 1));
    }
    setLocalStorageLinks(blockedLinks);
};

setInterval(fetchData, 5000);

BS.tabs.onActivated.addListener(async () => {
    const { id, url } = await getCurrentTab();
    console.log(id, url);
    await checkForBlockedLinks(id, url);
});

BS.tabs.onUpdated.addListener(async (id, _, { url }) => {
    console.log(id, url);
    await checkForBlockedLinks(id, url);
});
