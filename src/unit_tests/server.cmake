# Find the lancs shared library
find_library(lancsext NAMES liblancsext.so HINTS "${SRC_FOLDER}")
set(uname "Linux")

if(NOT lancsext)
    message(FATAL_ERROR "liblancsext not found! Aborting...")
endif()

# Add compiling flags
add_compile_options(-ggdb -O0 -g -coverage -DTEST_SERVER -DENABLE_AUDIT -DINOTIFY_ENABLED -fsanitize=address -fsanitize=undefined)
link_libraries(-fsanitize=address -fsanitize=undefined)
# Set tests dependencies
set(TEST_DEPS ${LANCSLIB} ${lancsext} -lpthread -ldl -lcmocka -fprofile-arcs -ftest-coverage)

add_subdirectory(analysisd)
add_subdirectory(remoted)
add_subdirectory(lancs_db)
add_subdirectory(os_auth)
add_subdirectory(os_crypto)
add_subdirectory(lancs_modules)
add_subdirectory(monitord)
add_subdirectory(logcollector)
add_subdirectory(os_execd)
