/* Copyright (C) 2015, Lancs Inc.
 * Copyright (C) 2009 Trend Micro Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License (version 2) as published by the FSF - Free Software
 * Foundation.
 */

#ifndef WIN_32UI_H
#define WIN_32UI_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <windows.h>
#include <winresrc.h>
#include <commctrl.h>

/* Default values */
#define CONFIG          "lancsnet.conf"
#define LASTCONFIG      "last-lancsnet.conf"
#define VERSION_FILE    "VERSION"
#define REVISION_FILE   "REVISION"
#define LANCSNETLOGS       "lancsnet.log"
#define HELPTXT         "help.txt"
#define SENDER_FILE     "rids\\sender_counter"
#define DEFDIR          "C:\\Program Files\\lancsnet-agent"
#define MOFILE          "m_file.txt"
/* Status messages */
#define ST_RUNNING          "Running"
#define ST_RUNNING_RESTART  "Running (pending restart)"
#define ST_STOPPED          "Stopped"
#define ST_UNKNOWN          "Unknown"
#define ST_NOTSET           "0"
#define ST_MISSING_IMPORT   "Require import of authentication key.\r\n" \
                            "            - Not Running"
#define ST_MISSING_SERVER   "Require Manager IP address.\r\n" \
                            "            - Not Running"
#define ST_MISSING_ALL      "Require import of authentication key.\r\n" \
                            "            Missing Manager IP address.\r\n" \
                            "            - Not Running"
#define ST_MISSING_LINKSAFE   "Require Linksafe ID.\r\n" \
                            "            - Not Running"
                      
/* Pre-def fields */
#define FL_NOKEY        "<insert_auth_key_here>"
#define FL_NOSERVER     "<insert_server_ip_here>"
#define FL_NOLSID       "<insert_linksafe_id_here>" // no linksafe id =))
#define FL_NOFILE       "<insert_file_here>"
#define FL_NONAME       "<insert_host_name>"
#define SERVER_IP_USED      1
#define SERVER_HOST_USED    2

/* Global lancsnet config structure */
typedef struct _lancsnet_config {
    unsigned short int server_type;
    unsigned short int admin_access;
    unsigned long int msg_sent;
    char *dir;
    char *config;
    char *key;
    char *server;
    char *linksafe_id;
    char *file_monitor;
    char *host_name;

    char *agentid;
    char *agentname;
    char *agentip;


    char *version;
    char *revision;
    char *status;
} lancsnet_config;


/** Global variables **/

/* Configuration */
extern lancsnet_config config_inst;

/* Status bar */
extern HWND hStatus;

/* Lancsnet icon */
#define IDI_LANCSNETICON  201
#define UI_MANIFEST_ID 202

/* User input */
#define UI_SERVER_TEXT      1501
#define UI_SERVER_AUTH      1502
#define UI_SERVER_MSG       1503
#define UI_SERVER_TOP       1504
#define UI_SERVER_INFO      1505
#define UI_ID_CLOSE         1510
#define UI_SERVER_LABEL     1506
#define UI_AGENT_FILE       1511
#define UI_HOST_NAME        1512

/* Menu values */
#define UI_MENU_MANAGE_STOP     1601
#define UI_MENU_MANAGE_START    1602
#define UI_MENU_MANAGE_STATUS   1603
#define UI_MENU_MANAGE_RESTART  1604
#define UI_MENU_MANAGE_EXIT     1605
#define UI_MENU_VIEW_LOGS       1606
#define UI_MENU_VIEW_CONFIG     1607
#define UI_MENU_HELP_HELP       1608
#define UI_MENU_HELP_ABOUT      1609
#define UI_MENU_NONE            1610

#define IDD_MAIN                1700
#define IDC_MAIN_STATUS         1701
#define IDC_ADD                 1702
#define IDC_CANCEL              1703
#define IDD_ABOUT               1704
#define IDC_STATIC              -1
#define IDC_ADD_F               1777
#define IDC_DEL                 1778
#define IDC_SHOW                1779
/** Prototypes **/

/* Generate server info */
int gen_server_info(HWND hwnd);

char *cat_file(char *file, FILE *fp2);

int is_file(char *file);

/* Reads lancsnet config */
int config_read(HWND hwnd);

/* Initializes the config */
void init_config();

/* Run command using cmd.exe */
int run_cmd(char *cmd, HWND hwnd);

/* Set LANCSNET Server IP */
int set_lancsnet_server(char *ip, HWND hwnd);

/* Set LANCSNET Linksafe ID*/
int set_lancsnet_linksafe_id(char *ip , HWND hwnd);

/* Set LANCSNET Host Name*/
int set_lancsnet_host_name(char *host_name, HWND hwnd);

/* Set file monitor*/
int set_file_monitor(char *path , HWND hwnd);

/* Delete file monitor*/
int del_file_monitor(char *path, HWND hwnd);

/* Set LANCSNET Auth Key */
int set_lancsnet_key(char *key, HWND hwnd);

/* Get LANCSNET Server IP */
int get_lancsnet_server();

/*Get LANCSNET Linksafe ID */
int get_lancsnet_linksafe_id();

/*Get LANCSNET Host Name*/
int get_lancsnet_host_name();

int _delete_from_mon_file(const char *infile, const char *outfile,char *path);
int Delete_from_mon_file(char *path,HWND hwnd);
int Delete_from_conf(const char *infile, const char *outfile,char *path);
int WriteToConf(const char *infile, const char *outfile,char *path);
int WriteToFileShow(char *path,char *buf);

#endif
