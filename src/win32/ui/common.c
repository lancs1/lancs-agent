/* Copyright (C) 2015, Lancs Inc.
 * Copyright (C) 2009 Trend Micro Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License (version 2) as published by the FSF - Free Software
 * Foundation.
 */

#include "shared.h"
#include "os_win32ui.h"
#include "../os_win.h"
#include "os_xml/os_xml.h"
#include "os_net/os_net.h"
#include "validate_op.h"
#define BUF_MAX 1024
#define MAXSIZE 10240
#define MATCHSTR "<syscheck>\0"
#define STRINGINSR "  <directories check_all=\"yes\" report_changes=\"yes\" realtime=\"yes\">%s</directories>\n"
char *trimString(char *str);
/* Agent status */
char ui_server_info[2048 + 1];

/* Generate server info (for the main status) */
int gen_server_info(HWND hwnd)
{
    memset(ui_server_info, '\0', 2048 + 1);
    snprintf(ui_server_info, 2048,
             "Agent: %s (%s)  -  %s\r\n\r\n"
             "Status: %s",
             config_inst.agentname,
             config_inst.agentid,
             config_inst.agentip,
             config_inst.status);

    /* Initialize top */
    if (config_inst.version)
    {
        SetDlgItemText(hwnd, UI_SERVER_TOP, config_inst.version);
        SetDlgItemText(hwnd, UI_SERVER_INFO, ui_server_info);
    }

    /* Initialize auth key */
    SetDlgItemText(hwnd, UI_SERVER_AUTH, config_inst.key);

    /* Initialize server IP */
    SetDlgItemText(hwnd, UI_SERVER_TEXT, config_inst.server);

    /* Initialize Linksafe ID */
    SetDlgItemText(hwnd, UI_SERVER_LABEL, config_inst.linksafe_id);

    /* Initialize FILE_MONITOR*/
    SetDlgItemText(hwnd, UI_AGENT_FILE, config_inst.file_monitor);

    /* Initialize HOST_NAME*/
    SetDlgItemText(hwnd, UI_HOST_NAME, config_inst.host_name);
    /* Set status data */
    SendMessage(hStatus, SB_SETTEXT, 0, (LPARAM) "https://lancs.com");
    if (config_inst.revision)
    {
        SendMessage(hStatus, SB_SETTEXT, 1, (LPARAM)config_inst.revision);
    }

    return (0);
}

/* Read the first line of a specific file  --must free after */
char *cat_file(char *file, FILE *fp2)
{
    FILE *fp;

    if (!fp2)
    {
        fp = fopen(file, "r");
    }
    else
    {
        fp = fp2;
    }

    if (fp)
    {
        char buf[1024 + 1];
        char *ret = NULL;

        buf[1024] = '\0';
        if (fgets(buf, 1024, fp) != NULL)
        {
            ret = strchr(buf, '\n');
            if (ret)
            {
                *ret = '\0';
            }
            ret = strchr(buf, '\r');
            if (ret)
            {
                *ret = '\0';
            }

            ret = strdup(buf);
        }

        if (!fp2)
        {
            fclose(fp);
        }
        return (ret);
    }

    return (NULL);
}

/* Check if a file exists */
int is_file(char *file)
{
    FILE *fp;
    fp = fopen(file, "r");
    if (fp)
    {
        fclose(fp);
        return (1);
    }
    return (0);
}
/*Trim off string */
char *trimString(char *str)
{
    char *end;

    while (isspace((unsigned char)*str))
        str++;

    if (*str == 0)
        return str;

    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end))
        end--;

    end[1] = '\0';

    return str;
}

/* Clear configuration */
void config_clear()
{
    if (config_inst.version)
    {
        free(config_inst.version);
    }

    if (config_inst.key)
    {
        free(config_inst.key);
    }

    if (config_inst.agentid)
    {
        free(config_inst.agentid);
    }

    if (config_inst.server)
    {
        free(config_inst.server);
    }

    if (config_inst.linksafe_id)
    {
        free(config_inst.linksafe_id);
    }

    if (config_inst.host_name)
    {
        free(config_inst.host_name);
    }

    if (config_inst.file_monitor)
    {
        free(config_inst.file_monitor);
    }

    if (config_inst.revision)
    {
        free(config_inst.revision);
    }

    if (config_inst.agentname)
    {
        free(config_inst.agentname);
    }

    if (config_inst.agentip)
    {
        free(config_inst.agentip);
    }

    /* Initialize config instance */
    config_inst.dir = NULL;
    config_inst.key = strdup(FL_NOKEY);
    config_inst.server = strdup(FL_NOSERVER);
    config_inst.config = NULL;
    config_inst.linksafe_id = strdup(FL_NOLSID);
    config_inst.file_monitor = strdup(FL_NOFILE);
    config_inst.host_name = strdup(FL_NONAME);
    config_inst.agentid = NULL;
    config_inst.agentname = NULL;
    config_inst.agentip = NULL;

    config_inst.version = NULL;
    config_inst.revision = NULL;
    config_inst.status = ST_UNKNOWN;
    config_inst.msg_sent = 0;
}

/* Initialize the config */
void init_config()
{
    /* Initialize config instance */
    config_inst.dir = NULL;
    config_inst.key = strdup(FL_NOKEY);
    config_inst.server = NULL;
    config_inst.config = NULL;
    config_inst.linksafe_id = strdup(FL_NOLSID);
    config_inst.file_monitor = strdup(FL_NOFILE);
    config_inst.host_name = strdup(FL_NONAME);
    config_inst.agentid = NULL;
    config_inst.agentname = NULL;
    config_inst.agentip = NULL;

    config_inst.version = NULL;
    config_inst.revision = NULL;
    config_inst.status = ST_UNKNOWN;
    config_inst.msg_sent = 0;
    config_inst.admin_access = 1;

    /* Check if ui is on the right path and has the proper permissions */
    if (!is_file(CONFIG))
    {
        if (chdir(DEFDIR))
        {
            config_inst.admin_access = 0;
        }

        if (!is_file(CONFIG))
        {
            config_inst.admin_access = 0;
        }
    }
}

/* Read lancsnet config */
int config_read(__attribute__((unused)) HWND hwnd)
{
    char *tmp_str;
    char buffer[4096];

    /* Clear config */
    config_clear();

    /* Get LANCSNET status */
    if (CheckServiceRunning())
    {
        config_inst.status = ST_RUNNING;
    }
    else
    {
        config_inst.status = ST_STOPPED;
    }

    /* Get version/revision */

    if (tmp_str = cat_file(VERSION_FILE, NULL), tmp_str)
    {
        snprintf(buffer, sizeof(buffer), "Lancs %s", tmp_str);
        os_strdup(buffer, config_inst.version);
    }

    free(tmp_str);
    if (tmp_str = cat_file(REVISION_FILE, NULL), tmp_str)
    {
        snprintf(buffer, sizeof(buffer), "Revision %s", tmp_str);
        os_strdup(buffer, config_inst.revision);
    }

    free(tmp_str);
    /* Get number of messages sent */
    tmp_str = cat_file(SENDER_FILE, NULL);
    if (tmp_str)
    {
        unsigned long int tmp_val = 0;
        char *to_free = tmp_str;

        tmp_val = atol(tmp_str);
        if (tmp_val)
        {
            config_inst.msg_sent = tmp_val * 9999;

            tmp_str = strchr(tmp_str, ':');
            if (tmp_str)
            {
                tmp_str++;
                tmp_val = atol(tmp_str);
                config_inst.msg_sent += tmp_val;
            }
        }

        free(to_free);
    }

    /* Get agent ID, name and IP */
    tmp_str = cat_file(KEYS_FILE, NULL);
    if (tmp_str)
    {
        char *to_free = tmp_str;
        /* Get base 64 */
        free(config_inst.key);
        config_inst.key = encode_base64(strlen(tmp_str), tmp_str);
        if (config_inst.key == NULL)
        {
            config_inst.key = strdup(FL_NOKEY);
        }

        /* Get ID */
        config_inst.agentid = tmp_str;

        tmp_str = strchr(tmp_str, ' ');
        if (tmp_str)
        {
            *tmp_str = '\0';
            tmp_str++;

            /* Get name */
            config_inst.agentname = tmp_str;
            tmp_str = strchr(tmp_str, ' ');
            if (tmp_str)
            {
                *tmp_str = '\0';
                tmp_str++;

                /* Get IP */
                config_inst.agentip = tmp_str;

                tmp_str = strchr(tmp_str, ' ');
                if (tmp_str)
                {
                    *tmp_str = '\0';
                }
            }

            config_inst.agentid = strdup(config_inst.agentid);
            config_inst.agentname = strdup(config_inst.agentname);
            config_inst.agentip = strdup(config_inst.agentip);
        }
        free(to_free);
    }

    if (config_inst.agentip == NULL)
    {
        config_inst.agentid = strdup(ST_NOTSET);
        config_inst.agentname = strdup("Auth key not imported.");
        config_inst.agentip = strdup(ST_NOTSET);

        config_inst.status = ST_MISSING_IMPORT;
    }

    /* Get server IP */
    if (!get_lancsnet_server())
    {
        if (strcmp(config_inst.status, ST_MISSING_IMPORT) == 0)
        {
            config_inst.status = ST_MISSING_ALL;
        }
        else
        {
            config_inst.status = ST_MISSING_SERVER;
        }
    }

    if (!get_lancsnet_linksafe_id())
    {
        if (strcmp(config_inst.status, ST_MISSING_IMPORT) == 0)
        {
            config_inst.status = ST_MISSING_ALL;
        }
        else
        {
            config_inst.status = ST_MISSING_LINKSAFE;
        }
    }

    if (!get_lancsnet_host_name())
    {
        if (strcmp(config_inst.status, ST_MISSING_IMPORT) == 0)
        {
            config_inst.status = ST_MISSING_ALL;
        }
        else
        {
            config_inst.status = ST_MISSING_LINKSAFE;
        }
    }

    return (0);
}

/* Get LANCSNET Server IP */
int get_lancsnet_server()
{
    OS_XML xml;
    char *str = NULL;
    int success = 0;

    /* Definitions */
    const char *(xml_serverip[]) = {"lancsnet_config", "client", "server-ip", NULL};
    const char *(xml_serverhost[]) = {"lancsnet_config", "client", "server-hostname", NULL};
    const char *(xml_serveraddr[]) = {"lancsnet_config", "client", "server", "address", NULL};

    /* Read XML */
    if (OS_ReadXML(CONFIG, &xml) < 0)
    {
        return (0);
    }

    /* We need to remove the entry for the server */
    if (config_inst.server)
    {
        free(config_inst.server);
        config_inst.server = NULL;
    }
    config_inst.server_type = 0;

    /* Get IP address of manager */
    if (str = OS_GetOneContentforElement(&xml, xml_serveraddr), str)
    {
        if (OS_IsValidIP(str, NULL) == 1)
        {
            config_inst.server_type = SERVER_IP_USED;
            config_inst.server = str;
            success = 1;
            goto ret;
        }
        else
        {
            /* If we don't find the IP, get the server hostname */
            config_inst.server_type = SERVER_HOST_USED;
            config_inst.server = str;
            success = 1;
            goto ret;
        }
    }
    if (str = OS_GetOneContentforElement(&xml, xml_serverip), str)
    {
        if (OS_IsValidIP(str, NULL) == 1)
        {
            config_inst.server_type = SERVER_IP_USED;
            config_inst.server = str;
            success = 1;
            goto ret;
        }
    }
    if (str = OS_GetOneContentforElement(&xml, xml_serverhost), str)
    {
        config_inst.server_type = SERVER_HOST_USED;
        config_inst.server = str;
        success = 1;
        goto ret;
    }

    /* Set up final server name when not available */
    config_inst.server = strdup(FL_NOSERVER);

ret:
    OS_ClearXML(&xml);
    return success;
}

/*Get LANCSNET Linksafe ID*/
int get_lancsnet_linksafe_id()
{
    OS_XML xml;
    char *str = NULL;
    int success = 0;

    /* Definitions */
    const char *(xml_label[]) = {"lancsnet_config", "labels", "label", NULL};

    /* Read XML */
    if (OS_ReadXML(CONFIG, &xml) < 0)
    {
        return (0);
    }

    if (config_inst.linksafe_id)
    {
        free(config_inst.linksafe_id);
        config_inst.linksafe_id = NULL;
    }

    /* Get LINKSAFE ID */
    if (str = OS_GetOneContentforElement(&xml, xml_label), str)
    {
        config_inst.linksafe_id = str;
        success = 1;
        goto ret;
    }
    config_inst.linksafe_id = strdup(FL_NOLSID);

ret:
    OS_ClearXML(&xml);
    return success;
}
/*Get LANCSNET Host Name*/
int get_lancsnet_host_name()
{
    OS_XML xml;
    char *str = NULL;
    int success = 0;

    /* Definitions */
    const char *(xml_label[]) = {"lancsnet_config", "client", "enrollment", "agent_name", NULL};

    /* Read XML */
    if (OS_ReadXML(CONFIG, &xml) < 0)
    {
        return (0);
    }

    if (config_inst.host_name)
    {
        free(config_inst.host_name);
        config_inst.host_name = NULL;
    }

    /* Get LINKSAFE ID */
    if (str = OS_GetOneContentforElement(&xml, xml_label), str)
    {
        config_inst.host_name = str;
        success = 1;
        goto ret;
    }
    config_inst.host_name = strdup(FL_NONAME);

ret:
    OS_ClearXML(&xml);
    return success;
}

/* Run a cmd.exe command */
int run_cmd(char *cmd, HWND hwnd)
{
    int result;
    int cmdlen;
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    DWORD exit_code;

    /* Build command */
    cmdlen = strlen(COMSPEC) + 5 + strlen(cmd);
    char finalcmd[cmdlen];
    snprintf(finalcmd, cmdlen, "%s /c %s", COMSPEC, cmd);

    /* Log command being run */
    mferror("Running the following command (%s)", finalcmd);

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    if (!CreateProcess(NULL, finalcmd, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL,
                       &si, &pi))
    {
        MessageBox(hwnd, "Unable to run command.",
                   "Error -- Failure Running Command", MB_OK);
        return (0);
    }

    /* Wait until process exits */
    WaitForSingleObject(pi.hProcess, INFINITE);

    /* Get exit code from command */
    result = GetExitCodeProcess(pi.hProcess, &exit_code);

    /* Close process and thread */
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    if (!result)
    {
        MessageBox(hwnd, "Could not determine exit code from command.",
                   "Error -- Failure Running Command", MB_OK);

        return (0);
    }

    return (exit_code);
}

/* Set LANCSNET Server IP */
int set_lancsnet_server(char *ip, HWND hwnd)
{
    const char **xml_pt = NULL;
    const char *(xml_serveraddr[]) = {"lancsnet_config", "client", "server", "address", NULL};
    char config_tmp[] = CONFIG;
    char *conf_file = basename_ex(config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];

    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    /* Verify IP Address */
    if (OS_IsValidIP(ip, NULL) != 1)
    {

        if (strchr(ip, '/'))
        {
            MessageBox(hwnd,
                       "A valid hostname cannot contain the following character: /",
                       "Cannot save hostname", MB_OK | MB_ICONERROR);
            return (0);
        }
        config_inst.server_type = SERVER_HOST_USED;
        xml_pt = xml_serveraddr;
    }
    else
    {
        config_inst.server_type = SERVER_IP_USED;
        xml_pt = xml_serveraddr;
    }

    /* Create temporary file */
    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting IP", MB_OK);
        return (0);
    }

    /* Read the XML. Print error and line number. */
    if (OS_WriteXML(CONFIG, tmp_path, xml_pt, NULL, ip) != 0)
    {
        MessageBox(hwnd, "Unable to set LANCSNET Server IP Address.\r\n"
                         "(Internal error on the XML Write).",
                   "Error -- Failure Setting IP", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    /* Rename config files */
    if (rename_ex(CONFIG, LASTCONFIG))
    {
        MessageBox(hwnd, "Unable to backup configuration.",
                   "Error -- Failure Backing Up Configuration", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, CONFIG))
    {
        MessageBox(hwnd, "Unable rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    return (1);
}
int _delete_from_mon_file(const char *infile, const char *outfile, char *path)
{
    FILE *fp_in;
    FILE *fp_out;

    /* Open infile */
    fp_in = fopen(infile, "r");
    if (!fp_in)
    {
        return (XMLW_NOIN);
    }

    setvbuf(fp_in, NULL, _IOFBF, MAXSIZE + 1);

    /* Open outfile */
    fp_out = fopen(outfile, "w");
    if (!fp_out)
    {
        fclose(fp_in);
        return (XMLW_NOOUT);
    }
    bool keep_reading = true;
    char temp_buf[BUF_MAX];
    char buffer[BUF_MAX];
    do
    {
        fgets(buffer, BUF_MAX, fp_in);
        memcpy(temp_buf, buffer, sizeof(buffer));
        char *res = trimString(temp_buf);
        if (feof(fp_in))
            keep_reading = false;
        else if (strcmp(res, path) == 0)
        {
            continue;
        }
        else
            fputs(buffer, fp_out);
    } while (keep_reading);
    fclose(fp_in);
    fclose(fp_out);
    return (0);
}
int Delete_from_mon_file(char *path, HWND hwnd)
{
    char config_tmp[] = MOFILE;
    char *conf_file = basename_ex(config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];
    /*create a temp path*/
    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    /*make a temp path */
    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting IP", MB_OK);
        return (0);
    }
    if (_delete_from_mon_file(MOFILE, tmp_path, path) != 0)
    {

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }
        return (0);
    }
    if (rename_ex(tmp_path, MOFILE))
    {
        MessageBox(hwnd, "Unable rename temporary file(monitor).",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file(monitor).",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    return (1);
}
int Delete_from_conf(const char *infile, const char *outfile, char *path)
{
    FILE *fp_in;
    FILE *fp_out;

    /* Open infile */
    fp_in = fopen(infile, "r");
    if (!fp_in)
    {
        return (XMLW_NOIN);
    }

    setvbuf(fp_in, NULL, _IOFBF, MAXSIZE + 1);

    /* Open outfile */
    fp_out = fopen(outfile, "w");
    if (!fp_out)
    {
        fclose(fp_in);
        return (XMLW_NOOUT);
    }
    bool keep_reading = true;
    char buffer[BUF_MAX];
    char temp_buf[BUF_MAX];
    char stringdel[1024];
    sprintf(stringdel, "<directories check_all=\"yes\" report_changes=\"yes\" realtime=\"yes\">%s</directories>", path);
    do
    {
        fgets(buffer, BUF_MAX, fp_in);
        memcpy(temp_buf, buffer, sizeof(buffer));
        char *res = trimString(temp_buf);
        if (feof(fp_in))
            keep_reading = false;
        else if (strcmp(res, stringdel) == 0)
        {
            continue;
        }
        else
            fputs(buffer, fp_out);
    } while (keep_reading);
    fclose(fp_in);
    fclose(fp_out);
    return (0);
}
int del_file_monitor(char *path, HWND hwnd)
{
    /*set up 2 file : lancsnet.conf vs tmp/lancsnet.confXXX*/
    char config_tmp[] = CONFIG;
    char *conf_file = basename_ex(config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];
    /*create a temp path*/
    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    /*make a temp path */
    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting IP", MB_OK);
        return (0);
    }
    /*Write to file config */
    if (Delete_from_conf(CONFIG, tmp_path, path) != 0)
    {
        MessageBox(hwnd, "Unable to set file to monitor.\r\n"
                         "(Internal error on the XML Write).",
                   "Error -- Failure Setting File Monitor", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }
        return (0);
    }
    /* Rename config files */
    if (rename_ex(CONFIG, LASTCONFIG))
    {
        MessageBox(hwnd, "Unable to backup configuration.",
                   "Error -- Failure Backing Up Configuration", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, CONFIG))
    {
        MessageBox(hwnd, "Unable rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }
    /*Delete from MOFILE*/
    Delete_from_mon_file(path, hwnd);
    return (1);
}

int WriteToConf(const char *infile, const char *outfile, char *path)
{
    FILE *fp_in;
    FILE *fp_out;
    FILE *fp_check;
    /* Open infile */
    fp_check = fopen(MOFILE,"r");
    if(!fp_check){
        return (XMLW_NOIN);
    }

    fp_in = fopen(infile, "r");
    if (!fp_in)
    {
        return (XMLW_NOIN);
    }

    setvbuf(fp_in, NULL, _IOFBF, MAXSIZE + 1);

    /* Open outfile */
    fp_out = fopen(outfile, "w");
    if (!fp_out)
    {
        fclose(fp_in);
        return (XMLW_NOOUT);
    }
    bool keep_reading = true;
    char buffer[BUF_MAX];
    char temp_buf[BUF_MAX];
    char line[BUF_MAX];
    bool check = false;
    do
    {
        fgets(buffer, BUF_MAX, fp_in); /*Read file config line by line*/
        memcpy(temp_buf, buffer, sizeof(buffer));
        char *temp_buff = trimString(temp_buf);
        if (feof(fp_in))
            keep_reading = false;
        else
            fputs(buffer, fp_out);
        if (strcmp(temp_buff, MATCHSTR) == 0)
        {
            while(fgets(line, BUF_MAX, fp_check)){
                char *tmp_line = trimString(line);
                if(!strcmp(tmp_line, path)){
                    check = true;
                    break;
                }
            }
            if(!check){
                fprintf(fp_out, STRINGINSR, path);
            }
        }
    } while (keep_reading);

    fclose(fp_check);
    fclose(fp_in);
    fclose(fp_out);

    if(!check){
        WriteToFileShow(MOFILE, path);
    }
    return (0);
}

int WriteToFileShow(char *path, char *buf)
{
    FILE *fp = fopen(path, "a");
    if (!fp)
    {
        return (1);
    }
    fprintf(fp, "%s\n", buf);
    // memset(buf,0,1024);
    fclose(fp);
}
int set_file_monitor(char *path, HWND hwnd)
{
    /*set up 2 file : lancsnet.conf vs tmp/lancsnet.confXXX*/
    char config_tmp[] = CONFIG;
    char *conf_file = basename_ex(config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];
    /*create a temp path*/
    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    /*make a temp path */
    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting IP", MB_OK);
        return (0);
    }
    /*Write to file config */
    if (WriteToConf(CONFIG, tmp_path, path) != 0)
    {
        MessageBox(hwnd, "Unable to set file to monitor.\r\n"
                         "(Internal error on the XML Write).",
                   "Error -- Failure Setting File Monitor", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }
        return (0);
    }
    /* Rename config files */
    if (rename_ex(CONFIG, LASTCONFIG))
    {
        MessageBox(hwnd, "Unable to backup configuration.",
                   "Error -- Failure Backing Up Configuration", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, CONFIG))
    {
        MessageBox(hwnd, "Unable rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }
    /*Write to file m_file to show if button Show is press*/

    return (1);
}

int set_lancsnet_linksafe_id(char *linksafeid, HWND hwnd)
{
    const char **xml_pt = NULL;
    const char *(xml_label[]) = {"lancsnet_config", "labels", "label", NULL};
    char config_tmp[] = CONFIG;
    char *conf_file = basename_ex(config_tmp);

    // char t_config_tmp[] = MOFILE;
    // char *t_conf_file = basename_ex(t_config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];

    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    xml_pt = xml_label;

    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting LINKSAFE ID", MB_OK);
        return (0);
    }

    /* Read the XML. Print error and line number. */
    if (OS_WriteXML(CONFIG, tmp_path, xml_pt, NULL, linksafeid) != 0)
    {
        MessageBox(hwnd, "Unable to set LANCSNET Linksafe ID.\r\n"
                         "(Internal error on the XML Write).",
                   "Error -- Failure Setting LINKSAFE ID", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    /* Rename config files */
    if (rename_ex(CONFIG, LASTCONFIG))
    {
        MessageBox(hwnd, "Unable to backup configuration.",
                   "Error -- Failure Backing Up Configuration", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, CONFIG))
    {
        MessageBox(hwnd, "Unable rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }
    return (1);
}

/*Set LANCSNET Host Name*/
int set_lancsnet_host_name(char *host_name, HWND hwnd)
{
    const char **xml_pt = NULL;
    const char *(xml_agent_name[]) = {"lancsnet_config", "client", "enrollment", "agent_name", NULL};

    char config_tmp[] = CONFIG;
    char *conf_file = basename_ex(config_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(conf_file) + 6 + 1];

    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, conf_file);

    xml_pt = xml_agent_name;

    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting Host Name", MB_OK);
        return (0);
    }

    /* Read the XML. Print error and line number. */
    if (OS_WriteXML(CONFIG, tmp_path, xml_pt, NULL, host_name) != 0)
    {
        MessageBox(hwnd, "Unable to set LANCSNET Host Name.\r\n"
                         "(Internal error on the XML Write).",
                   "Error -- Failure Setting HOST NAME", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    /* Rename config files */
    if (rename_ex(CONFIG, LASTCONFIG))
    {
        MessageBox(hwnd, "Unable to backup configuration.",
                   "Error -- Failure Backing Up Configuration", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, CONFIG))
    {
        MessageBox(hwnd, "Unable rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    return (1);
}

/* Set LANCSNET Authentication Key */
int set_lancsnet_key(char *key, HWND hwnd)
{
    FILE *fp;

    char auth_file_tmp[] = KEYS_FILE;
    char *keys_file = basename_ex(auth_file_tmp);

    char tmp_path[strlen(TMP_DIR) + 1 + strlen(keys_file) + 6 + 1];

    snprintf(tmp_path, sizeof(tmp_path), "%s/%sXXXXXX", TMP_DIR, keys_file);

    /* Create temporary file */
    if (mkstemp_ex(tmp_path) == -1)
    {
        MessageBox(hwnd, "Could not create temporary file.",
                   "Error -- Failure Setting IP", MB_OK);
        return (0);
    }

    fp = fopen(tmp_path, "w");
    if (fp)
    {
        fprintf(fp, "%s", key);
        fclose(fp);
    }
    else
    {
        MessageBox(hwnd, "Could not open temporary file for write.",
                   "Error -- Failure Importing Key", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    if (rename_ex(tmp_path, KEYS_FILE))
    {
        MessageBox(hwnd, "Unable to rename temporary file.",
                   "Error -- Failure Renaming Temporary File", MB_OK);

        if (unlink(tmp_path))
        {
            MessageBox(hwnd, "Could not delete temporary file.",
                       "Error -- Failure Deleting Temporary File", MB_OK);
        }

        return (0);
    }

    return (1);
}
