@echo Enable log
wevtutil sl Microsoft-Windows-AppXDeploymentServer/Operational /e:true
wevtutil sl Microsoft-Windows-AppxPackaging/Operational /e:true
wevtutil sl Microsoft-Windows-Bits-Client/Operational /e:true
wevtutil sl Microsoft-Windows-CodeIntegrity/Operational /e:true
wevtutil sl Microsoft-Windows-Diagnosis-Scripted/Operational /e:true
wevtutil sl Microsoft-Windows-DNS-Server/Analytical /e:true
wevtutil sl Microsoft-Windows-DriverFrameworks-UserMode/Operational /e:true
wevtutil sl Microsoft-Windows-Windows Firewall With Advanced Security/Firewall /e:true
wevtutil sl Microsoft-Windows-LSA/Operational /e:true
wevtutil sl Microsoft-Windows-NTLM/Operational /e:true
wevtutil sl OpenSSH/Operational /e:true
wevtutil sl Microsoft-Windows-PrintService/Operational /e:true
wevtutil sl Microsoft-Windows-PrintService/Admin /e:true
wevtutil sl Microsoft-Windows-Security-Mitigations/KernelMode /e:true
wevtutil sl Microsoft-Windows-Security-Mitigations/UserMode /e:true
wevtutil sl Microsoft-Windows-Shell-Core/Operational /e:true
wevtutil sl Microsoft-Windows-SmbClient/Security /e:true
wevtutil sl Microsoft-Windows-TaskScheduler/Operational /e:true
wevtutil sl Microsoft-Windows-TerminalServices-LocalSessionManager/Operational /e:true
wevtutil sl Microsoft-Windows-WMI-Activity/Operational /e:true
wevtutil sl Microsoft-Windows-Sysmon/Operational /e:true
@echo Install Sysmon
cd C:\Program Files (x86)\lancsnet-agent\tmp
curl https://download.sysinternals.com/files/Sysmon.zip -o Sysmon.zip
mkdir Sysmon
tar -xf Sysmon.zip -C Sysmon
curl https://raw.githubusercontent.com/SwiftOnSecurity/sysmon-config/master/sysmonconfig-export.xml -o ./Sysmon/sysmonconfig-export.xml
cd Sysmon
sysmon -u
sysmon -accepteula -i sysmonconfig-export.xml
cd ..
del Sysmon.zip
rmdir /s /q Sysmon
@echo Enable DNS
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome /v BuiltInDnsClientEnabled /t REG_DWORD /d 0
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update /v UpdateDefault /t REG_DWORD /d 1
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Edge /v BuiltInDnsClientEnabled /t REG_DWORD /d 0
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\EdgeUpdate /v UpdateDefault /t REG_DWORD /d 0
wevtutil sl Microsoft-Windows-PowerShell/Operational /e:true