# Backup dirs
$Env:LANCS_BACKUP_DIR         = ".\backup"
$TMP_BACKUP_DIR               = "lancs_backup_tmp"
# Finding MSI useful constants
$Env:LANCS_DEF_REG_START_PATH = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-18\Products\"
$Env:LANCS_PUBLISHER_VALUE    = "Lancs, Inc."

# Select powershell
if ((Get-WmiObject Win32_OperatingSystem).OSArchitecture -eq "64-bit" -And [System.IntPtr]::Size -eq 4) {
    write-output "$(Get-Date -format u) Sysnative Powershell will be used to access the registry." >> .\upgrade\upgrade.log
    Set-Alias Start-NativePowerShell "$env:windir\sysnative\WindowsPowerShell\v1.0\powershell.exe"
} else {
    Set-Alias Start-NativePowerShell "$env:windir\System32\WindowsPowerShell\v1.0\powershell.exe"
}

# Check unistall
function is_lancs_installed
{
    Start-NativePowerShell {

        $retval = $FALSE
        # Searching through the registry keys (Starting from $LANCS_DEF_REG_START_PATH)
        $path = Get-ChildItem $Env:LANCS_DEF_REG_START_PATH
        foreach ($subpaths in $path) {
            $subpath = $subpaths | Get-ChildItem
            foreach ($subsubpath in $subpath) {
                if ($subsubpath -match "InstallProperties") {
                    if ($subsubpath.GetValue("Publisher") -match $Env:LANCS_PUBLISHER_VALUE) {
                        $retval = $TRUE
                    }
                }
            }
        }

        Write-Output $retval
    }
}

# Forces Lancs-Agent to stop
function stop_lancs_agent
{
    param (
        $process_name
    )

    Get-Service -Name "Lancs" | Stop-Service -ErrorAction SilentlyContinue -Force
    $process_id = (Get-Process $process_name -ErrorAction SilentlyContinue).id
    $counter = 5

    while($process_id -ne $null -And $counter -gt 0)
    {
        write-output "$(Get-Date -format u) - Trying to stop Lancs service again. Remaining attempts: $counter." >> .\upgrade\upgrade.log
        $counter--
        Get-Service -Name "Lancs" | Stop-Service
        taskkill /pid $process_id /f /T
        Start-Sleep 2
        $process_id = (Get-Process $process_name -ErrorAction SilentlyContinue).id
    }

}

function backup_home
{
    write-output "$(Get-Date -format u) - Backing up Lancs home files." >> .\upgrade\upgrade.log

    # Clean before backup
    Remove-Item $Env:LANCS_BACKUP_DIR -recurse -ErrorAction SilentlyContinue -force
    Remove-Item $env:temp\$TMP_BACKUP_DIR -recurse -ErrorAction SilentlyContinue

    # Save lancs home in tmp dir (Exclude not filter directories)
    New-Item -ItemType directory -Path $env:temp\$TMP_BACKUP_DIR -ErrorAction SilentlyContinue
    Copy-Item .\*  $env:temp\$TMP_BACKUP_DIR -force

    # Move the tmp dir to local dir
    New-Item -ItemType directory -Path $Env:LANCS_BACKUP_DIR -ErrorAction SilentlyContinue
    Copy-Item $env:temp\$TMP_BACKUP_DIR\* $Env:LANCS_BACKUP_DIR -force
    Remove-Item $env:temp\$TMP_BACKUP_DIR -recurse -ErrorAction SilentlyContinue

}

function backup_msi {

    Start-NativePowerShell {

        write-output "$(Get-Date -format u) - Searching Lancs-Agent cached MSI through the registry." >> .\upgrade\upgrade.log

        $path = Get-ChildItem $Env:LANCS_DEF_REG_START_PATH
        $lancs_msi_path = $null

        # Searching through the registry keys (Starting from $LANCS_DEF_REG_START_PATH)
        foreach ($subpaths in $path) {
            $subpath = $subpaths | Get-ChildItem
            foreach ($subsubpath in $subpath) {
                if ($subsubpath -match "InstallProperties") {
                    if ($subsubpath.GetValue("Publisher") -match $Env:LANCS_PUBLISHER_VALUE) {
                        $lancs_msi_path = $subsubpath.GetValue("LocalPackage")
                    }
                }
            }
        }

        # Do backup the MSI if it exists
        if ($lancs_msi_path -ne $null) {
            $msi_filename = Split-Path $lancs_msi_path -leaf
            write-output "$(Get-Date -format u) - Backing up Lancs-Agent cached MSI: `"$lancs_msi_path`"." >> .\upgrade\upgrade.log
            Copy-Item $lancs_msi_path -Destination $Env:LANCS_BACKUP_DIR -force
            Write-Output "$msi_filename"
        } else {
            write-output "$(Get-Date -format u) - Lancs-Agent cached MSI was not found." >> .\upgrade\upgrade.log
        }
    }
}

# Looks for the Lancs-Agent uninstall command
function get_uninstall_string {

	Start-NativePowerShell {

        $UninstallString = $null
        # Searching through the registry keys (Starting from $LANCS_DEF_REG_START_PATH)
        $path = Get-ChildItem $Env:LANCS_DEF_REG_START_PATH
        foreach ($subpaths in $path) {
            $subpath = $subpaths | Get-ChildItem
            foreach ($subsubpath in $subpath) {
                if ($subsubpath -match "InstallProperties") {
                    if ($subsubpath.GetValue("Publisher") -match $Env:LANCS_PUBLISHER_VALUE) {
                        $UninstallString = $subsubpath.GetValue("UninstallString") + " /quiet"
                    }
                }
            }
        }
		Write-Output $UninstallString
	}
}

# Looks for the Lancs-Agent uninstall command and executes it, if exists
function uninstall_lancs {

    $UninstallString = get_uninstall_string

	if ($UninstallString -ne $null) {
		write-output "$(Get-Date -format u) - Performing the Lancs-Agent uninstall using: `"$UninstallString`"." >> .\upgrade\upgrade.log
		& "C:\Windows\SYSTEM32\cmd.exe" /c $UninstallString

		# registry takes some time to refresh (e.g.: NT 6.3)
		Start-Sleep 5
		$counter = 10
		While((is_lancs_installed) -And $counter -gt 0) {
			write-output "$(Get-Date -format u) - Waiting for the uninstallation to end." >> .\upgrade\upgrade.log
			$counter--
			Start-Sleep 2
		}
	} else {
		write-output "$(Get-Date -format u) - Lancs-Agent uninstall command was not found." >> .\upgrade\upgrade.log
	}

}

# Check new version and restart the Lancs service
function check-installation
{
    $new_version = (Get-Content VERSION)
    $counter = 5
    while($new_version -eq $current_version -And $counter -gt 0)
    {
        write-output "$(Get-Date -format u) - Waiting for the Lancs-Agent installation to end." >> .\upgrade\upgrade.log
        $counter--
        Start-Sleep 2
        $new_version = (Get-Content VERSION)
    }
    write-output "$(Get-Date -format u) - Restarting Lancs-Agent service." >> .\upgrade\upgrade.log
    Get-Service -Name "Lancs" | Start-Service
}

function restore
{
    param (
        $msi_filename
    )

    kill -processname win32ui -ErrorAction SilentlyContinue -Force
    stop_lancs_agent("lancs-agent")

    # Saves lancsnet.log before remove fail update
    Copy-Item $Env:LANCS_BACKUP_DIR\lancsnet.log $Env:LANCS_BACKUP_DIR\lancsnet.log.save -force
    Copy-Item lancsnet.log $Env:LANCS_BACKUP_DIR\lancsnet.log -force

    # Uninstall the latest version of the Lancs-Agent.
    uninstall_lancs

    # Install the former version of the Lancs-Agent
    if ($msi_filename -ne $null) {
        write-output "$(Get-Date -format u) - Excecuting former Lancs-Agent MSI: `"$Env:LANCS_BACKUP_DIR\$msi_filename`"." >> .\upgrade\upgrade.log
        cmd /c start $Env:LANCS_BACKUP_DIR\$msi_filename -quiet -norestart -log installer.log

        $counter = 10
        While(-Not (is_lancs_installed) -And $counter -gt 0) {
            write-output "$(Get-Date -format u) - Waiting for the installation to end." >> .\upgrade\upgrade.log
            $counter--
            Start-Sleep 2
        }
        Remove-Item $Env:LANCS_BACKUP_DIR\$msi_filename -ErrorAction SilentlyContinue
    }

    # Restore old files
    write-output "$(Get-Date -format u) - Restoring former Lancs-Agent home files." >> .\upgrade\upgrade.log
    Copy-Item $Env:LANCS_BACKUP_DIR\* .\ -force

    # Get current version
    $current_version = (Get-Content VERSION)
    write-output "$(Get-Date -format u) - Current version: $($current_version)." >> .\upgrade\upgrade.log
}

# Stop UI and launch the msi installer
function install
{
    kill -processname win32ui -ErrorAction SilentlyContinue -Force
    Remove-Item .\upgrade\upgrade_result -ErrorAction SilentlyContinue
    write-output "$(Get-Date -format u) - Starting upgrade processs." >> .\upgrade\upgrade.log
    cmd /c start /wait (Get-Item ".\lancs-agent*.msi").Name -quiet -norestart -log installer.log
}


# Get current version
$current_version = (Get-Content VERSION)
write-output "$(Get-Date -format u) - Current version: $($current_version)." > .\upgrade\upgrade.log

# Get process name
$current_process = "lancs-agent"
If (!(Test-Path ".\lancs-agent.exe"))
{
    $current_process = "lancsnet-agent"
}

# Generating backup
write-output "$(Get-Date -format u) - Generating backup." >> .\upgrade\upgrade.log
backup_home
$previous_msi_name = backup_msi

# Ensure implicated processes are stopped before launch the upgrade
Get-Process msiexec | Stop-Process -ErrorAction SilentlyContinue -Force
stop_lancs_agent($current_process)

# Install
install
check-installation
write-output "$(Get-Date -format u) - Installation finished." >> .\upgrade\upgrade.log

# Check process status
$process_id = (Get-Process lancs-agent).id
$counter = 5
while($process_id -eq $null -And $counter -gt 0)
{
    $counter--
    Start-Service -Name "Lancs"
    Start-Sleep 2
    $process_id = (Get-Process lancs-agent).id
}
write-output "$(Get-Date -format u) - Process ID: $($process_id)." >> .\upgrade\upgrade.log

# Wait for agent state to be cleaned
Start-Sleep 10

# Check status file
$status = Get-Content .\lancs-agent.state | select-string "status='connected'" -SimpleMatch
$counter = 5
while($status -eq $null -And $counter -gt 0)
{
    $counter--
    Start-Sleep 2
    $status = Get-Content .\lancs-agent.state | select-string "status='connected'" -SimpleMatch
}
write-output "$(Get-Date -format u) - Reading status file: $($status)." >> .\upgrade\upgrade.log

If ($status -eq $null)
{
    Get-Service -Name "Lancs" | Stop-Service
    write-output "$(Get-Date -format u) - Upgrade failed: Restoring former installation." >> .\upgrade\upgrade.log

    write-output "2" | out-file ".\upgrade\upgrade_result" -encoding ascii

    .\lancs-agent.exe uninstall-service >> .\upgrade\upgrade.log
    restore($previous_msi_name)

    If ($current_process -eq "lancs-agent")
    {
        write-output "$(Get-Date -format u) - Installing Lancs service." >> .\upgrade\upgrade.log
        .\lancs-agent.exe install-service >> .\upgrade\upgrade.log
    }
    Else
    {
        write-output "$(Get-Date -format u) - Installing Lancs service." >> .\upgrade\upgrade.log
        sc.exe delete LancsSvc -ErrorAction SilentlyContinue -Force
        Remove-Item .\lancs-agent.exe -ErrorAction SilentlyContinue
        Remove-Item .\lancs-agent.state -ErrorAction SilentlyContinue
        .\lancsnet-agent.exe install-service >> .\upgrade\upgrade.log
    }

    write-output "$(Get-Date -format u) - Starting Lancs-Agent service." >> .\upgrade\upgrade.log
    Start-Service -Name "Lancs" -ErrorAction SilentlyContinue

}
Else
{
    write-output "0" | out-file ".\upgrade\upgrade_result" -encoding ascii
    write-output "$(Get-Date -format u) - Upgrade finished successfully." >> .\upgrade\upgrade.log
    $new_version = (Get-Content VERSION)
    write-output "$(Get-Date -format u) - New version: $($new_version)." >> .\upgrade\upgrade.log
}

Remove-Item $Env:LANCS_BACKUP_DIR -recurse -ErrorAction SilentlyContinue
Remove-Item -Path ".\upgrade\*"  -Exclude "*.log", "upgrade_result" -ErrorAction SilentlyContinue
