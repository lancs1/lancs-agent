#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define chrome_src "C:\\Program Files (x86)\\lancsnet-agent\\extensions\\userChrome.js"
#define config "C:\\Program Files (x86)\\lancsnet-agent\\extensions\\channel_prefs.js"
#define chrome_des "C:\\Program Files\\Mozilla Firefox\\userChrome.js"
#define config_des "C:\\Program Files\\Mozilla Firefox\\defaults\\pref\\channel-prefs.js"

// write all content from path1 to path2
int w(char *path1, char *path2)
{

    FILE *fp1, *fp2;
    fp1 = fopen(path1, "r");
    fp2 = fopen(path2, "w");

    if (!fp1)
    {
        printf("Can not open %s file\n", path1);
        return (-1);
    }
    if (!fp2)
    {
        printf("Can not open %s file\n", path2);
        return (-1);
    }
    char *line = (char *)malloc(256);

    while (fgets(line, 256, fp1))
        fprintf(fp2, "%s", line);

    fclose(fp1);
    fclose(fp2);
    free(line);
    return 1;
}

int main()
{
    if (w(chrome_src, chrome_des) > 0)
        remove(chrome_src);
    if (w(config, config_des) > 0)
        remove(config);
    return 0;
}