#ifdef WIN32

#include "active_responses.h"
#define DEVMON "C:\\Program Files (x86)\\lancsnet-agent\\devcon.exe"

int main(int argc, char **argv)
{
    FILE *fp;
    fp = fopen("F:\\devmon.txt", "a");

    (void)argc;
    char log_msg[OS_MAXSTR];
    int action = OS_INVALID;
    cJSON *input_json = NULL;

    action = setup_and_check_message(argv, &input_json);
    if ((action != ADD_COMMAND) && (action != DELETE_COMMAND))
    {
        return OS_INVALID;
    }

    // Get srcip
    const char *srcid = get_srcid_from_json(input_json);
    fprintf(fp, "srcid: %s\n", srcid);
    if (!srcid)
    {
        write_debug_file(argv[0], "Cannot read 'srcid' from data");
        cJSON_Delete(input_json);
        return OS_INVALID;
    }

    if (action == ADD_COMMAND)
    {
        char **keys = NULL;
        int action2 = OS_INVALID;

        os_calloc(2, sizeof(char *), keys);
        os_strdup(srcid, keys[0]);
        fprintf(fp, "srdid: %s  keys[0]: %s", srcid, keys[0]);
        keys[1] = NULL;

        action2 = send_keys_and_check_message(argv, keys);

        os_free(keys);

        // If necessary, abort execution
        if (action2 != CONTINUE_COMMAND)
        {
            cJSON_Delete(input_json);

            if (action2 == ABORT_COMMAND)
            {
                write_debug_file(argv[0], "Aborted");
                return OS_SUCCESS;
            }
            else
            {
                return OS_INVALID;
            }
        }
    }

    char deviceID[OS_MAXSTR - 1];
    snprintf(deviceID, OS_MAXSTR - 1, "\"@%s\"", srcid);
    fprintf(fp, "devicdID: %s\n", deviceID);
    fclose(fp);

    // char *deviceId_test = "\"USB\\VID_0951&PID_1666\\E0D55EA574D7168188751C02\"";
    char *add[4] = {DEVMON, "disable", deviceID, NULL};
    char *delete[4] = {DEVMON, "enable", deviceID, NULL};

    wfd_t *wfd = wpopenv(DEVMON, (action == ADD_COMMAND) ? add : delete, W_BIND_STDERR);
    if (!wfd)
    {
        memset(log_msg, '\0', OS_MAXSTR);
        snprintf(log_msg, OS_MAXSTR - 1, "Unable to run devmon, action: '%s', deviceID: '%s'", (action == ADD_COMMAND) ? "ADD" : "DELETE", deviceID);
        write_debug_file(argv[0], log_msg);
    }
    else
    {
        wpclose(wfd);
    }

    write_debug_file(argv[0], "Ended");

    cJSON_Delete(input_json);
}

#endif
