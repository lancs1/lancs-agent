#!/bin/sh

#Copyright (C) 2015, Lancs Inc.
# Install functions for Lancs
# Lancs.com (https://github.com/lancs)

patch_version(){
        rm -rf $PREINSTALLEDDIR/etc/shared/ssh > /dev/null 2>&1
}
LancsSetup(){
    patch_version
}

InstallSELinuxPolicyPackage(){

    if command -v semodule > /dev/null && command -v getenforce > /dev/null; then
        if [ -f selinux/lancs.pp ]; then
            if [ $(getenforce) != "Disabled" ]; then
                cp selinux/lancs.pp /tmp && semodule -i /tmp/lancs.pp
                rm -f /tmp/lancs.pp
                semodule -e lancs
            fi
        fi
    fi
}

CheckModuleIsEnabled(){
    # This function requires a properly formatted lancsnet.conf.
    # It doesn't work if the configuration is set in the same line

    # How to use it:
    #
    # CheckModuleIsEnabled '<wodle name="open-scap">' '</wodle>' 'disabled'
    # CheckModuleIsEnabled '<cluster>' '</cluster>' 'disabled'
    # CheckModuleIsEnabled '<sca>' '</sca>' 'enabled'

    open_label="$1"
    close_label="$2"
    enable_label="$3"

    if grep -n "${open_label}" $PREINSTALLEDDIR/etc/lancsnet.conf > /dev/null ; then
        is_disabled="no"
    else
        is_disabled="yes"
    fi

    if [ "${enable_label}" = "disabled" ]; then
        tag="<disabled>"
        enabled_tag="${tag}no"
        disabled_tag="${tag}yes"
    else
        tag="<enabled>"
        enabled_tag="${tag}yes"
        disabled_tag="${tag}no"
    fi

    end_config_limit="99999999"
    for start_config in $(grep -n "${open_label}" $PREINSTALLEDDIR/etc/lancsnet.conf | cut -d':' -f 1); do
        end_config="$(sed -n "${start_config},${end_config_limit}p" $PREINSTALLEDDIR/etc/lancsnet.conf | sed -n "/${open_label}/,\$p" | grep -n "${close_label}" | head -n 1 | cut -d':' -f 1)"
        end_config="$((start_config + end_config))"

        if [ -n "${start_config}" ] && [ -n "${end_config}" ]; then
            configuration_block="$(sed -n "${start_config},${end_config}p" $PREINSTALLEDDIR/etc/lancsnet.conf)"

            for line in $(echo ${configuration_block} | grep -n "${tag}" | cut -d':' -f 1); do
                # Check if the component is enabled
                if echo ${configuration_block} | sed -n ${line}p | grep "${enabled_tag}" > /dev/null ; then
                    is_disabled="no"

                # Check if the component is disabled
                elif echo ${configuration_block} | sed -n ${line}p | grep "${disabled_tag}" > /dev/null; then
                    is_disabled="yes"
                fi
            done
        fi
    done

    echo ${is_disabled}
}

LancsUpgrade()
{
    # Encode Agentd passlist if not encoded

    passlist=$PREINSTALLEDDIR/agentless/.passlist

    if [ -f $passlist ] && ! base64 -d $passlist > /dev/null 2>&1; then
        cp $passlist $passlist.bak
        base64 $passlist.bak > $passlist

        if [ $? = 0 ]; then
            echo "Agentless passlist encoded successfully."
            rm -f $passlist.bak
        else
            echo "ERROR: Couldn't encode Agentless passlist."
            mv $passlist.bak $passlist
        fi
    fi

    # Remove/relocate existing SQLite databases
    rm -f $PREINSTALLEDDIR/var/db/.profile.db*
    rm -f $PREINSTALLEDDIR/var/db/.template.db*
    rm -f $PREINSTALLEDDIR/var/db/agents/*

    if [ -f "$PREINSTALLEDDIR/var/db/global.db" ]; then
        cp $PREINSTALLEDDIR/var/db/global.db $PREINSTALLEDDIR/queue/db/
        if [ -f "$PREINSTALLEDDIR/queue/db/global.db" ]; then
            chmod 640 $PREINSTALLEDDIR/queue/db/global.db
            chown lancs:lancs $PREINSTALLEDDIR/queue/db/global.db
            rm -f $PREINSTALLEDDIR/var/db/global.db*
        else
            echo "Unable to move global.db during the upgrade"
        fi
    fi

    # Remove existing SQLite databases for Lancs DB, only if upgrading from 3.2..3.6

    MAJOR=$(echo $USER_OLD_VERSION | cut -dv -f2 | cut -d. -f1)
    MINOR=$(echo $USER_OLD_VERSION | cut -d. -f2)

    if [ $MAJOR = 3 ] && [ $MINOR -lt 7 ]
    then
        rm -f $PREINSTALLEDDIR/queue/db/*.db*
    fi
    rm -f $PREINSTALLEDDIR/queue/db/.template.db

    # Remove existing SQLite databases for vulnerability-detector

    rm -f $PREINSTALLEDDIR/wodles/cve.db
    rm -f $PREINSTALLEDDIR/queue/vulnerabilities/cve.db

    # Migrate .agent_info and .wait files before removing deprecated socket folder

    if [ -d $PREINSTALLEDDIR/queue/lancsnet ]; then
        if [ -f $PREINSTALLEDDIR/queue/lancsnet/.agent_info ]; then
            mv -f $PREINSTALLEDDIR/queue/lancsnet/.agent_info $PREINSTALLEDDIR/queue/sockets/.agent_info
        fi
        if [ -f $PREINSTALLEDDIR/queue/lancsnet/.wait ]; then
            mv -f $PREINSTALLEDDIR/queue/lancsnet/.wait $PREINSTALLEDDIR/queue/sockets/.wait
        fi
        rm -rf $PREINSTALLEDDIR/queue/lancsnet
    fi

    # Move rotated logs to new folder and remove the existing one

    if [ -d $PREINSTALLEDDIR/logs/lancsnet ]; then
        if [ "$(ls -A $PREINSTALLEDDIR/logs/lancsnet)" ]; then
            mv -f $PREINSTALLEDDIR/logs/lancsnet/* $PREINSTALLEDDIR/logs/lancs
        fi
        rm -rf $PREINSTALLEDDIR/logs/lancsnet
    fi

    # Remove deprecated Lancs tools

    rm -f $PREINSTALLEDDIR/bin/lancsnet-control
    rm -f $PREINSTALLEDDIR/bin/lancsnet-regex
    rm -f $PREINSTALLEDDIR/bin/lancsnet-logtest
    rm -f $PREINSTALLEDDIR/bin/lancsnet-makelists
    rm -f $PREINSTALLEDDIR/bin/util.sh
    rm -f $PREINSTALLEDDIR/bin/rootcheck_control
    rm -f $PREINSTALLEDDIR/bin/syscheck_control
    rm -f $PREINSTALLEDDIR/bin/syscheck_update

    # Remove old Lancs daemons

    rm -f $PREINSTALLEDDIR/bin/lancsnet-agentd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-agentlessd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-analysisd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-authd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-csyslogd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-dbd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-execd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-integratord
    rm -f $PREINSTALLEDDIR/bin/lancsnet-logcollector
    rm -f $PREINSTALLEDDIR/bin/lancsnet-maild
    rm -f $PREINSTALLEDDIR/bin/lancsnet-monitord
    rm -f $PREINSTALLEDDIR/bin/lancsnet-remoted
    rm -f $PREINSTALLEDDIR/bin/lancsnet-reportd
    rm -f $PREINSTALLEDDIR/bin/lancsnet-syscheckd

    # Remove existing ruleset version file

    rm -f $PREINSTALLEDDIR/ruleset/VERSION

    # Remove old Active Response scripts

    rm -f $PREINSTALLEDDIR/active-response/bin/firewall-drop.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/default-firewall-drop.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/pf.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/npf.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/ipfw.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/ipfw_mac.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/firewalld-drop.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/disable-account.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/host-deny.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/ip-customblock.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/restart-lancsnet.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/route-null.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/kaspersky.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/lancsnet-slack.sh
    rm -f $PREINSTALLEDDIR/active-response/bin/lancsnet-tweeter.sh

    # Remove deprecated lancsnet-init.conf file and its link
    if [ -f /etc/lancsnet-init.conf ]; then
        rm -f $PREINSTALLEDDIR/etc/lancsnet-init.conf
        rm -f /etc/lancsnet-init.conf
    fi

    # Replace and delete lancsnet group along with lancsnet users
    LANCSNET_GROUP=lancsnet
    if (grep "^lancsnet:" /etc/group > /dev/null 2>&1) || (dscl . -read /Groups/lancsnet > /dev/null 2>&1)  ; then
        find $PREINSTALLEDDIR -group $LANCSNET_GROUP -user root -exec chown root:lancs {} \;
        find $PREINSTALLEDDIR -group $LANCSNET_GROUP -exec chown lancs:lancs {} \;
    fi
    ./src/init/delete-oldusers.sh $LANCSNET_GROUP

    # Remove unnecessary `execa` socket
    if [ -f "$DIRECTORY/queue/alerts/execa" ]; then
        rm -f $DIRECTORY/queue/alerts/execa
    fi
}
