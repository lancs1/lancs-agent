#!/usr/bin/env bash

mkdir -p /var/lancsnet/stats/totals/2019/Aug/
cp -rf /tmp/configuration_files/lancsnet-totals-27.log /var/lancsnet/stats/totals/2019/Aug/lancsnet-totals-27.log
chown -R lancs:lancs /var/lancsnet/stats/totals/2019/Aug/lancsnet-totals-27.log
