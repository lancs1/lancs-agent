import os
import socket
import sys
sys.path.append('/tools')

from healthcheck_utils import get_manager_health_base, check

if __name__ == "__main__":
    # Workers are not needed in this test, so the exit code is set to 0 (healthy).
    exit(check(os.system(
        "grep -q 'lancs-syscheckd: INFO: (6009): File integrity monitoring scan ended.' /var/lancsnet/logs/lancsnet.log"))
         or get_manager_health_base()) if socket.gethostname() == 'lancs-master' else exit(0)
