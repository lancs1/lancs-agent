#!/usr/bin/env bash

if [ "$HOSTNAME" == "lancs-master" ]; then
  sed -i -e "/<cluster>/,/<\/cluster>/ s|<disabled>[a-z]\+</disabled>|<disabled>yes</disabled>|g" /var/lancsnet/etc/lancsnet.conf
  rm -rf /var/lancsnet/stats/totals/*
  mkdir -p /var/lancsnet/stats/totals/2019/Aug/
  cp -rf /tmp/configuration_files/lancsnet-totals-27.log /var/lancsnet/stats/totals/2019/Aug/lancsnet-totals-27.log
  chown -R lancs:lancs /var/lancsnet/stats/totals/2019/Aug/lancsnet-totals-27.log
fi
