import os
import sys

sys.path.append('/tools')

from healthcheck_utils import get_agent_health_base, check

if __name__ == "__main__":
    exit(check(os.system("grep -q 'lancs-modulesd:syscollector.*INFO: Evaluation finished.' /var/lancsnet/logs/lancsnet.log"))
         or get_agent_health_base())
