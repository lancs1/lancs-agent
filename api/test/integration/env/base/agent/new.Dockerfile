FROM public.ecr.aws/o5x5t0j3/amd64/api_development:integration_test_lancs-generic

ARG LANCS_BRANCH

## install Lancs
RUN mkdir lancs && curl -sL https://github.com/lancs/lancs/tarball/${LANCS_BRANCH} | tar zx --strip-components=1 -C lancs
ADD base/agent/preloaded-vars.conf /lancs/etc/preloaded-vars.conf
RUN /lancs/install.sh

COPY base/agent/entrypoint.sh /scripts/entrypoint.sh

HEALTHCHECK --retries=900 --interval=1s --timeout=30s --start-period=30s CMD /usr/bin/python3 /tmp/healthcheck/healthcheck.py || exit 1
