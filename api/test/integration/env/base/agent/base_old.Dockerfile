FROM ubuntu:18.04

RUN apt-get update && apt-get install -y curl apt-transport-https lsb-release gnupg2
RUN curl -s https://packages.lancs.com/key/GPG-KEY-LANCS | apt-key add - && \
    echo "deb https://packages.lancs.com/3.x/apt/ stable main" | tee /etc/apt/sources.list.d/lancs.list && \
    apt-get update && apt-get install lancs-agent=3.13.2-1 -y
