#!/usr/bin/env bash

# Enable debug mode for the modulesd daemon
echo 'lancs_modules.debug=2' >> /var/lancsnet/etc/local_internal_options.conf

# Apply test.keys
cp /tmp/configuration_files/test.keys /var/lancsnet/etc/test.keys

# Remove lancsnet_4.x in agents with version 3.x
if [ "$3" == "agent_old" ]; then
  rm /tmp/configuration_files/lancsnet_4.x.conf
fi

# Modify lancsnet.conf
for conf_file in /tmp/configuration_files/*.conf; do
  python3 /tools/xml_parser.py /var/lancsnet/etc/lancsnet.conf $conf_file
done

sed -n "/$2 /p" /var/lancsnet/etc/test.keys > /var/lancsnet/etc/client.keys
chown root:lancs /var/lancsnet/etc/client.keys
rm /var/lancsnet/etc/test.keys

# Agent configuration
for sh_file in /tmp/configuration_files/*.sh; do
  . $sh_file
done

/var/lancsnet/bin/lancs-control start || /var/lancsnet/bin/lancsnet-control start

tail -f /var/lancsnet/logs/lancsnet.log
