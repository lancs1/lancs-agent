FROM public.ecr.aws/o5x5t0j3/amd64/api_development:integration_test_lancs-generic

# INSTALL MANAGER
ARG LANCS_BRANCH

ADD base/manager/supervisord.conf /etc/supervisor/conf.d/

RUN mkdir lancs && curl -sL https://github.com/lancs/lancs/tarball/${LANCS_BRANCH} | tar zx --strip-components=1 -C lancs
COPY base/manager/preloaded-vars.conf /lancs/etc/preloaded-vars.conf
RUN /lancs/install.sh

COPY base/manager/entrypoint.sh /scripts/entrypoint.sh

# HEALTHCHECK
HEALTHCHECK --retries=900 --interval=1s --timeout=30s --start-period=30s CMD /var/lancsnet/framework/python/bin/python3 /tmp/healthcheck/healthcheck.py || exit 1
