#!/usr/bin/env bash

# Apply API configuration
cp -rf /tmp/config/* /var/lancsnet/ && chown -R lancs:lancs /var/lancsnet/api

# Modify lancsnet.conf
for conf_file in /tmp/configuration_files/*.conf; do
  python3 /tools/xml_parser.py /var/lancsnet/etc/lancsnet.conf $conf_file
done

sed -i "s:<key>key</key>:<key>9d273b53510fef702b54a92e9cffc82e</key>:g" /var/lancsnet/etc/lancsnet.conf
sed -i "s:<node>NODE_IP</node>:<node>$1</node>:g" /var/lancsnet/etc/lancsnet.conf
sed -i "s:<node_name>node01</node_name>:<node_name>$2</node_name>:g" /var/lancsnet/etc/lancsnet.conf
sed -i "s:validate_responses=False:validate_responses=True:g" /var/lancsnet/api/scripts/lancs-apid.py

if [ "$3" != "master" ]; then
    sed -i "s:<node_type>master</node_type>:<node_type>worker</node_type>:g" /var/lancsnet/etc/lancsnet.conf
fi

cp -rf /tmp/configuration_files/config/* /var/lancsnet/
chown root:lancs /var/lancsnet/etc/client.keys
chown -R lancs:lancs /var/lancsnet/queue/agent-groups
chown -R lancs:lancs /var/lancsnet/queue/db
chown -R lancs:lancs /var/lancsnet/etc/shared
chmod --reference=/var/lancsnet/etc/shared/default /var/lancsnet/etc/shared/group*
cd /var/lancsnet/etc/shared && find -name merged.mg -exec chown lancs:lancs {} \; && cd /
chown root:lancs /var/lancsnet/etc/shared/ar.conf

sleep 1

# Manager configuration
for py_file in /tmp/configuration_files/*.py; do
  /var/lancsnet/framework/python/bin/python3 $py_file
done

for sh_file in /tmp/configuration_files/*.sh; do
  . $sh_file
done

echo "" > /var/lancsnet/logs/api.log
/var/lancsnet/bin/lancs-control start

# Master-only configuration
if [ "$3" == "master" ]; then
  for py_file in /tmp/configuration_files/master_only/*.py; do
    /var/lancsnet/framework/python/bin/python3 $py_file
  done

  for sh_file in /tmp/configuration_files/master_only/*.sh; do
    . $sh_file
  done

  exit_flag=0
  [ -e /entrypoint_error ] && rm -f /entrypoint_error
  # Wait until Lancs API is ready
  elapsed_time=0
  while [[ $(grep -c 'Listening on' /var/lancsnet/logs/api.log)  -eq 0 ]] && [[ $exit_flag -eq 0 ]]
  do
    if [ $elapsed_time -gt 300 ]; then
      echo "Timeout on API callback. Could not find 'Listening on'" > /entrypoint_error
      exit_flag=1
    fi
    sleep 1
    elapsed_time=$((elapsed_time+1))
  done

  # RBAC configuration
  for sql_file in /tmp/configuration_files/*.sql; do
    # Redirect standard error to /tmp/sql_lock_check to check a possible locked database error
    # 2>&1 redirects "standard error" to "standard output"
    sqlite3 /var/lancsnet/api/configuration/security/rbac.db < $sql_file > /tmp/sql_lock_check 2>&1

    # Insert the RBAC configuration again if database was locked
    elapsed_time=0
    while [[ $(grep -c 'database is locked' /tmp/sql_lock_check)  -eq 1 ]] && [[ $exit_flag -eq 0 ]]
    do
      if [ $elapsed_time -gt 120 ]; then
        echo "Timeout on RBAC DB callback. Could not apply SQL file to RBAC DB" > /entrypoint_error
        exit_flag=1
      fi
      sleep 1
      elapsed_time=$((elapsed_time+1))
      sqlite3 /var/lancsnet/api/configuration/security/rbac.db < $sql_file > /tmp/sql_lock_check 2>&1
    done

    # Remove the temporal file used to check the possible locked database error
    rm -rf /tmp/sql_lock_check
  done
fi

/usr/bin/supervisord
