from unittest.mock import patch

with patch('lancs.common.lancs_uid'):
    with patch('lancs.common.lancs_gid'):
        from lancs.core.results import AffectedItemsLancsResult


class CustomAffectedItems(AffectedItemsLancsResult):
    """Mock custom values that are needed in controller tests"""

    def __init__(self, empty: bool = False):
        if not empty:
            super().__init__(dikt={'dikt_key': 'dikt_value'},
                             affected_items=[{'id': '001'}])
        else:
            super().__init__()

    def __getitem__(self, key):
        return self.render()[key]
