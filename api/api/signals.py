# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2


async def modify_response_headers(request, response):
    # Delete 'Server' entry
    response.headers.pop('Server', None)
