# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

import os

from lancs.core import common

API_PATH = os.path.join(common.lancs_path, 'api')
CONFIG_PATH = os.path.join(API_PATH, 'configuration')
CONFIG_FILE_PATH = os.path.join(CONFIG_PATH, 'api.yaml')
RELATIVE_CONFIG_FILE_PATH = os.path.relpath(CONFIG_FILE_PATH, common.lancs_path)
SECURITY_PATH = os.path.join(CONFIG_PATH, 'security')
SECURITY_CONFIG_PATH = os.path.join(SECURITY_PATH, 'security.yaml')
RELATIVE_SECURITY_PATH = os.path.relpath(SECURITY_PATH, common.lancs_path)
API_LOG_FILE_PATH = os.path.join(common.lancs_path, 'logs', 'api.log')
API_SSL_PATH = os.path.join(CONFIG_PATH, 'ssl')
