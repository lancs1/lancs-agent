import json
from unittest.mock import patch

import pytest
from api.models.configuration_model import HTTPSModel

with patch('lancs.common.lancs_uid'):
    with patch('lancs.common.lancs_gid'):
        from api.encoder import prettify, dumps
        from lancs.core.results import LancsResult


def custom_hook(dct):
    if 'key' in dct:
        return HTTPSModel.from_dict(dct)
    elif 'error' in dct:
        return LancsResult.decode_json({'result': dct, 'str_priority': 'v2'})
    else:
        return dct


@pytest.mark.parametrize('o', [HTTPSModel(key='v1'),
                               LancsResult({'k1': 'v1'}, str_priority='v2')
                               ]
                         )
def test_lancsjsonencoder_dumps(o):
    """Test dumps method from API encoder using LancsJSONEncoder."""
    encoded = dumps(o)
    decoded = json.loads(encoded, object_hook=custom_hook)
    assert decoded == o


def test_lancsjsonencoder_prettify():
    """Test prettify method from API encoder using LancsJSONEncoder."""
    assert prettify({'k1': 'v1'}) == '{\n   "k1": "v1"\n}'
