# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

import os
import sys
from copy import deepcopy
from unittest.mock import patch, MagicMock, ANY, call

from werkzeug.exceptions import Unauthorized

with patch('lancs.core.common.lancs_uid'):
    with patch('lancs.core.common.lancs_gid'):
        from lancs.core.results import LancsResult

import pytest

with patch('lancs.core.common.lancs_uid'):
    with patch('lancs.core.common.lancs_gid'):
        sys.modules['lancs.rbac.orm'] = MagicMock()
        from api import authentication

        del sys.modules['lancs.rbac.orm']

test_path = os.path.dirname(os.path.realpath(__file__))
test_data_path = os.path.join(test_path, 'data')

security_conf = LancsResult({
    'auth_token_exp_timeout': 900,
    'rbac_mode': 'black'
})
decoded_payload = {
    "iss": 'lancs',
    "aud": 'Lancs API REST',
    "nbf": 0,
    "exp": security_conf['auth_token_exp_timeout'],
    "sub": '001',
    "rbac_policies": {'value': 'test', 'rbac_mode': security_conf['rbac_mode']},
    "rbac_roles": [1],
    'run_as': False
}

original_payload = {
    "iss": 'lancs',
    "aud": 'Lancs API REST',
    "nbf": 0,
    "exp": security_conf['auth_token_exp_timeout'],
    "sub": '001',
    'run_as': False,
    "rbac_roles": [1],
    "rbac_mode": security_conf['rbac_mode']
}


def test_check_user_master():
    result = authentication.check_user_master('test_user', 'test_pass')
    assert result == {'result': True}


@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.__init__', return_value=None)
@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.distribute_function', side_effect=None)
@patch('concurrent.futures.ThreadPoolExecutor.submit', side_effect=None)
@patch('api.authentication.raise_if_exc', side_effect=None)
def test_check_user(mock_raise_if_exc, mock_submit, mock_distribute_function, mock_dapi):
    """Verify if result is as expected"""
    result = authentication.check_user('test_user', 'test_pass')

    assert result == {'sub': 'test_user', 'active': True}, 'Result is not as expected'
    mock_dapi.assert_called_once_with(f=ANY, f_kwargs={'user': 'test_user', 'password': 'test_pass'},
                                      request_type='local_master', is_async=False, wait_for_complete=False, logger=ANY)
    mock_distribute_function.assert_called_once_with()
    mock_raise_if_exc.assert_called_once()


@patch('api.authentication.change_keypair', return_value=('-----BEGIN PRIVATE KEY-----',
                                                          '-----BEGIN PUBLIC KEY-----'))
@patch('os.chmod')
@patch('os.chown')
@patch('builtins.open')
def test_generate_keypair(mock_open, mock_chown, mock_chmod, mock_change_keypair):
    """Verify correct params when calling open method inside generate_keypair"""
    result = authentication.generate_keypair()
    assert result == ('-----BEGIN PRIVATE KEY-----',
                      '-----BEGIN PUBLIC KEY-----')

    calls = [call(authentication._private_key_path, authentication.lancs_uid(), authentication.lancs_gid()),
             call(authentication._public_key_path, authentication.lancs_uid(), authentication.lancs_gid())]
    mock_chown.assert_has_calls(calls)
    calls = [call(authentication._private_key_path, 0o640),
             call(authentication._public_key_path, 0o640)]
    mock_chmod.assert_has_calls(calls)

    with patch('os.path.exists', return_value=True):
        authentication.generate_keypair()
        calls = [call(authentication._private_key_path, mode='r'),
                 call(authentication._public_key_path, mode='r')]
        mock_open.assert_has_calls(calls, any_order=True)


def test_generate_keypair_ko():
    """Verify expected exception is raised when IOError"""
    with patch('builtins.open'):
        with patch('os.chmod'):
            with patch('os.chown', side_effect=PermissionError):
                assert authentication.generate_keypair()


@patch('builtins.open')
def test_change_keypair(mock_open):
    """Verify correct params when calling open method inside change_keypair"""
    result = authentication.change_keypair()
    assert isinstance(result[0], str)
    assert isinstance(result[1], str)
    calls = [call(authentication._private_key_path, mode='w'),
             call(authentication._public_key_path, mode='w')]
    mock_open.assert_has_calls(calls, any_order=True)


def test_get_security_conf():
    """Check that returned object is as expected"""
    result = authentication.get_security_conf()
    assert isinstance(result, dict)
    assert all(x in result.keys() for x in ('auth_token_exp_timeout', 'rbac_mode'))


@patch('api.authentication.time', return_value=0)
@patch('api.authentication.jwt.encode', return_value='test_token')
@patch('api.authentication.generate_keypair', return_value=('-----BEGIN PRIVATE KEY-----',
                                                            '-----BEGIN PUBLIC KEY-----'))
@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.__init__', return_value=None)
@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.distribute_function', side_effect=None)
@patch('concurrent.futures.ThreadPoolExecutor.submit', side_effect=None)
@patch('api.authentication.raise_if_exc', side_effect=None)
def test_generate_token(mock_raise_if_exc, mock_submit, mock_distribute_function, mock_dapi, mock_generate_keypair,
                        mock_encode, mock_time):
    """Verify if result is as expected"""
    mock_raise_if_exc.return_value = security_conf
    result = authentication.generate_token('001', {'roles': [1]})
    assert result == 'test_token', 'Result is not as expected'

    # Check all functions are called with expected params
    mock_dapi.assert_called_once_with(f=ANY, request_type='local_master', is_async=False, wait_for_complete=False,
                                      logger=ANY)
    mock_distribute_function.assert_called_once_with()
    mock_raise_if_exc.assert_called_once()
    mock_generate_keypair.assert_called_once()
    mock_encode.assert_called_once_with(original_payload, '-----BEGIN PRIVATE KEY-----',
                                        algorithm='ES512')


@patch('api.authentication.TokenManager')
def test_check_token(mock_tokenmanager):
    result = authentication.check_token(username='lancs_user', roles=tuple([1]), token_nbf_time=3600, run_as=False,
                                        origin_node_type='master')
    assert result == {'valid': ANY, 'policies': ANY}


@patch('api.authentication.jwt.decode')
@patch('api.authentication.generate_keypair', return_value=('-----BEGIN PRIVATE KEY-----',
                                                            '-----BEGIN PUBLIC KEY-----'))
@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.__init__', return_value=None)
@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.distribute_function', side_effect=None)
@patch('concurrent.futures.ThreadPoolExecutor.submit', side_effect=None)
@patch('api.authentication.raise_if_exc', side_effect=None)
def test_decode_token(mock_raise_if_exc, mock_submit, mock_distribute_function, mock_dapi, mock_generate_keypair,
                      mock_decode):
    mock_decode.return_value = deepcopy(original_payload)
    mock_raise_if_exc.side_effect = [LancsResult({'valid': True, 'policies': {'value': 'test'}}),
                                     LancsResult(security_conf)]

    result = authentication.decode_token('test_token')
    assert result == decoded_payload

    # Check all functions are called with expected params
    calls = [call(f=ANY, f_kwargs={'username': original_payload['sub'], 'token_nbf_time': original_payload['nbf'],
                                   'run_as': False, 'roles': tuple(original_payload['rbac_roles']),
                                   'origin_node_type': 'master'},
                  request_type='local_master', is_async=False, wait_for_complete=False, logger=ANY),
             call(f=ANY, request_type='local_master', is_async=False, wait_for_complete=False, logger=ANY)]
    mock_dapi.assert_has_calls(calls)
    mock_generate_keypair.assert_called_once()
    mock_decode.assert_called_once_with('test_token', '-----BEGIN PUBLIC KEY-----',
                                        algorithms=['ES512'],
                                        audience='Lancs API REST')
    assert mock_distribute_function.call_count == 2
    assert mock_raise_if_exc.call_count == 2


@patch('lancs.core.cluster.dapi.dapi.DistributedAPI.distribute_function', side_effect=None)
@patch('concurrent.futures.ThreadPoolExecutor.submit', side_effect=None)
@patch('api.authentication.raise_if_exc', side_effect=None)
@patch('api.authentication.generate_keypair', return_value=('-----BEGIN PRIVATE KEY-----',
                                                            '-----BEGIN PUBLIC KEY-----'))
def test_decode_token_ko(mock_generate_keypair, mock_raise_if_exc, mock_submit, mock_distribute_function):
    """Assert exceptions are handled as expected inside decode_token()"""
    with pytest.raises(Unauthorized):
        authentication.decode_token(token='test_token')

    with patch('api.authentication.jwt.decode') as mock_decode:
        with patch('api.authentication.generate_keypair',
                   return_value=('-----BEGIN PRIVATE KEY-----',
                                 '-----BEGIN PUBLIC KEY-----')):
            with patch('lancs.core.cluster.dapi.dapi.DistributedAPI.__init__', return_value=None):
                with patch('lancs.core.cluster.dapi.dapi.DistributedAPI.distribute_function'):
                    with patch('api.authentication.raise_if_exc') as mock_raise_if_exc:
                        mock_decode.return_value = deepcopy(original_payload)

                        with pytest.raises(Unauthorized):
                            mock_raise_if_exc.side_effect = [LancsResult({'valid': False})]
                            authentication.decode_token(token='test_token')

                        with pytest.raises(Unauthorized):
                            mock_raise_if_exc.side_effect = [
                                LancsResult({'valid': True, 'policies': {'value': 'test'}}),
                                LancsResult({'auth_token_exp_timeout': 900,
                                             'rbac_mode': 'white'})]
                            authentication.decode_token(token='test_token')
