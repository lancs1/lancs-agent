#!/usr/bin/env python

# Copyright (C) 2015, Lancs Inc.
# Created by Lancs, Inc. <info@lancs.com>.
# This program is a free software; you can redistribute it and/or modify it under the terms of GPLv2

from setuptools import setup, find_packages

# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

setup(
    name='api',
    version='4.3.10',
    description="Lancs API",
    author_email="hello@lancs.com",
    author="Lancs",
    url="https://github.com/lancs",
    keywords=["Lancs API"],
    install_requires=[],
    packages=find_packages(exclude=["*.test", "*.test.*", "test.*", "test"]),
    package_data={'': ['spec/spec.yaml']},
    include_package_data=True,
    zip_safe=False,
    license='GPLv2',
    long_description="""\
    The Lancs API is an open source RESTful API that allows for interaction with the Lancs manager from a web browser, command line tool like cURL or any script or program that can make web requests. The Lancs Kibana app relies on this heavily and Lancs’s goal is to accommodate complete remote management of the Lancs infrastructure via the Lancs Kibana app. Use the API to easily perform everyday actions like adding an agent, restarting the manager(s) or agent(s) or looking up syscheck details.
    """
)
