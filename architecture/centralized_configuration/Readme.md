<!---
Copyright (C) 2015, Lancs Inc.
Created by Lancs, Inc. <info@lancs.com>.
This program is free software; you can redistribute it and/or modify it under the terms of GPLv2
-->

# Centralized Configuration
## Index
- [Centralized Configuration](#centralized-configuration)
  - [Index](#index)
  - [Purpose](#purpose)
  - [Sequence diagram](#sequence-diagram)

## Purpose

One of the key features of Lancs as a EDR is the Centralized Configuration, allowing to deploy configurations, policies, rootcheck descriptions or any other file from Lancs Manager to any Lancs Agent based on their grouping configuration. This feature has multiples actors: Lancs Cluster (Master and Worker nodes), with `lancs-remoted` as the main responsible from the managment side, and Lancs Agent with `lancs-agentd` as resposible from the client side.


## Sequence diagram
Sequence diagram shows the basic flow of Centralized Configuration based on the configuration provided. There are mainly three stages:
1. Lancs Manager Master Node (`lancs-remoted`) creates every `remoted.shared_reload` (internal) seconds the files that need to be synchronized with the agents.
2. Lancs Cluster as a whole (via `lancs-clusterd`) continuously synchronize files between Lancs Manager Master Node and Lancs Manager Worker Nodes
3. Lancs Agent `lancs-agentd` (via ) sends every `notify_time` (lancsnet.conf) their status, being `merged.mg` hash part of it. Lancs Manager Worker Node (`lancs-remoted`) will check if agent's `merged.mg` is out-of-date, and in case this is true, the new `merged.mg` will be pushed to Lancs Agent.